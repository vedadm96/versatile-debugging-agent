#include "stm32f4xx.h"
#include "spi.h"

void initSPI1(uint16_t prescaler)
{/// init SPI1 in master mode
	///wwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwww
	/// SPI1 Pin configuration
	///-----------------------------------------------------------------
	/// PA4 = CS
	/// PA5 = SCK
	/// PA6 = MISO
	/// PA7 = MOSI
	///wwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwww
	
	RCC->AHB1ENR |= RCC_AHB1ENR_GPIOAEN; 								//
	GPIOA->MODER |= 0x0000A800; 										// 
	GPIOA->AFR[0] |= 0x55500000;										//   
	GPIOA->OSPEEDR |= 0x0000A800;										// 
	
	GPIOA->MODER |= GPIO_MODER_MODER4_0; 
	GPIOA->OTYPER &= ~(GPIO_OTYPER_OT4);
	GPIOA->OSPEEDR |= GPIO_OSPEEDR_OSPEED4_1;										// 

	SPI1_CS_HIGH;	
	RCC->APB2ENR |= RCC_APB2ENR_SPI1EN; 								// 	
	SPI1->CR1 = (SPI_CR1_MSTR);											// enable 8 bit data & master mode			
	SPI1->CR1 |= (SPI_CR1_SSI)|(SPI_CR1_SSM);
	// SPI1->CR1 |= (SPI_CR1_CPOL)|(SPI_CR1_CPHA);	
	SPI1->CR1 |= prescaler;		
	SPI1->CR1 |= (SPI_CR1_SPE);											// 
}
void initSPI2(uint16_t prescaler)
{/// init SPI2 in master mode
	///wwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwww
	/// SPI2 Pin configuration
	///-----------------------------------------------------------------
	/// PB12 = CS
	/// PA9 = SCK
	/// PC2 = MISO
	/// PC3 = MOSI
	///wwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwww
	
	RCC->AHB1ENR |= RCC_AHB1ENR_GPIOCEN; 								//
	GPIOC->MODER |= 0x000000A0; 										// 
	GPIOC->AFR[0] |= 0x00005500;										//   
	GPIOC->OSPEEDR |= 0x000000A0;										// 
	
	RCC->AHB1ENR |= RCC_AHB1ENR_GPIOAEN; 								//
	GPIOA->MODER |= 0x00080000; 										// 
	GPIOA->AFR[1] |= 0x00000050;										//   
	GPIOA->OSPEEDR |= 0x00080000;										// 
	
	RCC->AHB1ENR |= RCC_AHB1ENR_GPIOBEN; 								//
	GPIOB->MODER |= GPIO_MODER_MODER12_0; 
	GPIOB->OTYPER &= ~(GPIO_OTYPER_OT12);
	GPIOB->OSPEEDR |= GPIO_OSPEEDR_OSPEED12_1;										// 

	SPI2_CS_HIGH;	
	RCC->APB1ENR |= RCC_APB1ENR_SPI2EN; 								// 	
	SPI2->CR1 = (SPI_CR1_MSTR);											// enable 8 bit data & master mode			
	SPI2->CR1 |= (SPI_CR1_SSI)|(SPI_CR1_SSM);
	// SPI1->CR1 |= (SPI_CR1_CPOL)|(SPI_CR1_CPHA);	
	SPI2->CR1 |= prescaler;		
	SPI2->CR1 |= (SPI_CR1_SPE);											// 
}

uint8_t rxByteSPIx(SPI_TypeDef* spi)
{///  receive data using SPI1
	uint8_t data;
	
	spi->DR = 0x00;						 							// 
	while(!(spi->SR & SPI_I2S_FLAG_TXE)); 								// 
	while(!(spi->SR & SPI_I2S_FLAG_RXNE)); 							// 
	while(spi->SR & SPI_I2S_FLAG_BSY); 								//	
	data = spi->DR; 													//
	
	return data;
}	

uint8_t txByteSPIx(SPI_TypeDef* spi,uint8_t data)
{/// send data using SPI1
	uint8_t tmp;

	spi->DR = data; 													//
	while(!(spi->SR & SPI_I2S_FLAG_TXE));								// 
	while(!(spi->SR & SPI_I2S_FLAG_RXNE)); 							// 
	while(spi->SR & SPI_I2S_FLAG_BSY); 								// 
	tmp = spi->DR; 													// 
		
	return tmp;
}


uint8_t rxByteSPI1(void)
{///  receive data using SPI1
	uint8_t data;
	
	SPI1->DR = 0x00;						 							// 
	while(!(SPI1->SR & SPI_I2S_FLAG_TXE)); 								// 
	while(!(SPI1->SR & SPI_I2S_FLAG_RXNE)); 							// 
	while(SPI1->SR & SPI_I2S_FLAG_BSY); 								//	
	data = SPI1->DR; 													//
	
	return data;
}	

uint8_t txByteSPI1(uint8_t data)
{/// send data using SPI1
	uint8_t tmp;

	SPI1->DR = data; 													//
	while(!(SPI1->SR & SPI_I2S_FLAG_TXE));								// 
	while(!(SPI1->SR & SPI_I2S_FLAG_RXNE)); 							// 
	while(SPI1->SR & SPI_I2S_FLAG_BSY); 								// 
	tmp = SPI1->DR; 													// 
		
	return tmp;
}
uint8_t rxByteSPI2(void)
{///  receive data using SPI1
	uint8_t data;
	
	SPI2->DR = 0x00;						 							// 
	while(!(SPI2->SR & SPI_I2S_FLAG_TXE)); 								// 
	while(!(SPI2->SR & SPI_I2S_FLAG_RXNE)); 							// 
	while(SPI2->SR & SPI_I2S_FLAG_BSY); 								//	
	data = SPI2->DR; 													//
	
	return data;
}	

uint8_t txByteSPI2(uint8_t data)
{/// send data using SPI1
	uint8_t tmp;

	SPI2->DR = data; 													//
	while(!(SPI2->SR & SPI_I2S_FLAG_TXE));								// 
	while(!(SPI2->SR & SPI_I2S_FLAG_RXNE)); 							// 
	while(SPI2->SR & SPI_I2S_FLAG_BSY); 								// 
	tmp = SPI2->DR; 													// 
		
	return tmp;
}


void sendLED(){
	//32bit startframe
	txByteSPI1(0x00);
	txByteSPI1(0x00);	
	txByteSPI1(0x00);	
	txByteSPI1(0x00);	

	// //LED1-49
	for(uint8_t i=0;i<1;i++){
		txByteSPI1(0xe2);
		txByteSPI1(0xff);
		txByteSPI1(0xff);
		txByteSPI1(0xff);
	}
	//32bit endframe
	txByteSPI1(0xff);	
	txByteSPI1(0xff);	
	txByteSPI1(0xff);	
	txByteSPI1(0xff);	
}


uint16_t getRealPrescaler(uint8_t spiNum,uint16_t prescaler){

	uint16_t realScale=0;
	while(prescaler>2){
		realScale+=0x08;
		prescaler=prescaler>>1;
	}	
	return realScale; 
}