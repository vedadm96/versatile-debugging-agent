#ifndef __CONFIG_H_
#define __CONFIG_H_

#include "stm32f4xx.h"
#include "usart.h"

#define SYSTEM_DEBUG
/* #define INIT_USB_HS */ 
//#define BLE_DEBUG
/* #define FIRMWARE_UPDATE_TEST */

#define initUART										initUSART3
#define	printUART										printUSART3
#define UART_BAUDRATE									USART3_BAUDRATE_921600
#define putcharUART										putcharUSART3

#define UART_BUFFER_SIZE								1024							// must be in form of 2^n
#define UART_BUFFER_MASK								((UART_BUFFER_SIZE) - 1)					#define FIRMWARE_VERSION								0x55555555		// HWD Ver 1.5 -- FW Ver 1.1.14
#define BOOTLOADER_VERSION								0x55555555
#define FIRMWARE_ID										0x44515249

#define BOOT_REASON_FLAG_ADDR							0x40024000
#define BOOT_REASON_NORMAL								0x00000000
#define BOOT_REASON_INSTALL_LATEST_FW					0xA5B6C7D8
#define BOOT_REASON_INSTALL_INIT_FW						0xFEEDBEED

#define LED_COUNT										8

#define BLE_PACKET_SIZE									36
#define BLE_PAYLOAD_SIZE								33
#define BLE_CMD_MAX_PAYLOAD								(16*(BLE_PAYLOAD_SIZE))
#define BLE_SUPPORTED_CMD_COUNT							12 
#define BLE_CMD_STM_TX_LAST_PACKET						0x02
#define BLE_CMD_CONNECTION_STATE						0x03

#define BLE_CMD_LED_SET_COLOR							0x10
#define BLE_CMD_LED_OFF_ALL								0x11
#define BLE_CMD_LED_SET_ALL_COLOR						0x12
#define BLE_CMD_LED_SET_GLOBAL_INTENSITY				0x13

#define BLE_CMD_POWER_OFF_NRF52							0x41
#define BLE_CMD_FWUPDATE_INFO							0x50
#define BLE_CMD_FWUPDATE_BLOCK_REQ						0x51
#define BLE_CMD_FWUPDATE_BLOCK_REP						0x52
#define BLE_CMD_FWUPDATE_DONE							0x53

#define BLE_CMD_ACK										0x91


#define BLE_CMD_TIMER_DISABLED							0
#define BLE_CMD_TIMER_ENABLED							1


#define BLE_PKT_ERROR									0
#define BLE_PKT_OK										1

#define BLE_WAIT4CMD_RECEPTION							5000	// x1ms

#define BLE_CMD_ACK_SIZE								2

#define BLE_CODE_SUCCESS								0x06	//
#define BLE_ERROR_CODE_UNKNOWN_COMMAND					0x51	//
#define BLE_ERROR_CODE_INTERNAL_ERROR					0x50	
#define BLE_ERROR_CODE_MALFORMED_COMMAND				0x52	//
#define BLE_ERROR_CODE_PREV_COMMAND_NOT_ACKED			0x53
#define BLE_ERROR_CODE_CHECKSUM_ERROR					0x54	//
#define BLE_ERROR_CODE_PARAMETER_MISSING				0x55
#define BLE_ERROR_CODE_MALFORMED_PARAMETER				0x56	//
#define BLE_ERROR_CODE_WRONG_CHUNK						0x57	//
#define BLE_ERROR_CODE_CAN_NOT_EXE_IN_CURRENT_STATE		0x65



#define EFLASH_SECTOR_SIZE								0x00001000
#define EFLASH_BLOCK_SIZE								0x00010000
#define EFLASH_USER_DATA_ADDR							0x00000000
#define EFLASH_INITIAL_FIRMWARE_ADDR					0x00010000
#define ELFASH_IMAGE_SIZE								0x00080000
#define EFLASH_FIRMWARE_IMAGE1_ADDR						((EFLASH_INITIAL_FIRMWARE_ADDR)+(ELFASH_IMAGE_SIZE))
#define EFLASH_FIRMWARE_IMAGE2_ADDR						((EFLASH_FIRMWARE_IMAGE1_ADDR)+(ELFASH_IMAGE_SIZE))

#define FW_BUFFER_SIZE									2048
#define FW_BUFFER_MASK									((FW_BUFFER_SIZE)-1)
#define MAX_DATA_PER_BLOCK								476

///wwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwww
/// MIDI related constant and data types
///-----------------------------------------------------------------------------
#define MIDI_MSG_MAX_SIZE								64
#define MIDI_MSG_BUFFER_SIZE							128
#define MIDI_MSG_BUFFER_MASK							((MIDI_MSG_BUFFER_SIZE) - 1)

#define MIDI_MSG_NONE									0x00
#define MIDI_MSG_HS										0x01
#define MIDI_MSG_FS										0x02
#define MIDI_MSG_BLE									0x04

#define MIDI_HWD_DISABLED								0x00
#define MIDI_HWD_READY									0x01

typedef struct midi_msg_t
{
	uint8_t data[MIDI_MSG_MAX_SIZE];
	uint8_t len;
	uint8_t src;
	uint8_t dst;
} MIDI_MSG;

typedef struct midi_info_t
{
	MIDI_MSG msg[MIDI_MSG_BUFFER_SIZE];
	uint16_t widx;
	uint16_t ridx;
	
	uint8_t hwd_state;
		
} MIDI_INFO;

extern volatile MIDI_INFO g_MIDI_INFO;
//extern volatile MIDI_INFO g_FS_MIDI_INFO;
//extern volatile MIDI_INFO g_HS_MIDI_INFO;




typedef struct led_info_t
{
	uint16_t led;
	uint8_t mode;
	uint8_t state;
	uint32_t timer;
	uint32_t period;
	uint32_t on_time;
	
    GPIO_TypeDef * GPIOx;
} LED_INFO;


typedef struct uart_info_t
{
	uint8_t tx_state;
	uint8_t rx_state;
	
	uint32_t tx_timer;
	uint32_t rx_timer;
	
	uint8_t rx_buff[UART_BUFFER_SIZE];
	uint16_t rx_buff_widx;
	uint16_t rx_buff_ridx;
	
	uint8_t tx_buff[UART_BUFFER_SIZE];
	uint16_t tx_buff_widx;
	uint16_t tx_buff_ridx;
	
} UART_INFO;

typedef struct ble_info_t
{
	uint8_t cmd;
	uint8_t payload_size;
	uint8_t payload[BLE_PAYLOAD_SIZE];
	uint8_t last_packet[BLE_PACKET_SIZE];
	
	uint8_t cmd_timer_flag;
	uint32_t cmd_timer;
	
	uint16_t rx_buff_idx;
	uint8_t rx_buff[BLE_CMD_MAX_PAYLOAD];
	
} BLE_INFO;

typedef struct fw_info_t
{
	uint8_t init;
	uint16_t next_block;
	uint16_t block_buff_widx;
	
	uint8_t block_buff[FW_BUFFER_SIZE];
	uint8_t buff[FW_BUFFER_SIZE];
	uint16_t widx;
	uint16_t ridx;
	uint32_t proc_timer;
	uint32_t timer;
	uint32_t size;
	//uint32_t csum;
	uint8_t csum[4];
	uint32_t block_size;
	uint32_t rxed_bytes;
	uint32_t baddr;
} FW_INFO; 

extern volatile UART_INFO g_UART_INFO;
extern volatile BLE_INFO g_BLE_INFO;
extern volatile FW_INFO g_FW_INFO;
extern volatile LED_INFO g_LED_INFO[LED_COUNT];

#endif 
