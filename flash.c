#include "flash.h"

/**
@verbatim
 ===============================================================================
              ##### FLASH Interface configuration functions #####
 ===============================================================================
    [..]
      This group includes the following functions:
      (+) void FLASH_SetLatency(uint32_t FLASH_Latency)
          To correctly read data from FLASH memory, the number of wait states (LATENCY)
          must be correctly programmed according to the frequency of the CPU clock
          (HCLK) and the supply voltage of the device.
    [..]
      For STM32F405xx/07xx and STM32F415xx/17xx devices
 +-------------------------------------------------------------------------------------+
 | Latency       |                HCLK clock frequency (MHz)                           |
 |               |---------------------------------------------------------------------|
 |               | voltage range  | voltage range  | voltage range   | voltage range   |
 |               | 2.7 V - 3.6 V  | 2.4 V - 2.7 V  | 2.1 V - 2.4 V   | 1.8 V - 2.1 V   |
 |---------------|----------------|----------------|-----------------|-----------------|
 |0WS(1CPU cycle)|0 < HCLK <= 30  |0 < HCLK <= 24  |0 < HCLK <= 22   |0 < HCLK <= 20   |
 |---------------|----------------|----------------|-----------------|-----------------|
 |1WS(2CPU cycle)|30 < HCLK <= 60 |24 < HCLK <= 48 |22 < HCLK <= 44  |20 < HCLK <= 40  |
 |---------------|----------------|----------------|-----------------|-----------------|
 |2WS(3CPU cycle)|60 < HCLK <= 90 |48 < HCLK <= 72 |44 < HCLK <= 66  |40 < HCLK <= 60  |
 |---------------|----------------|----------------|-----------------|-----------------|
 |3WS(4CPU cycle)|90 < HCLK <= 120|72 < HCLK <= 96 |66 < HCLK <= 88  |60 < HCLK <= 80  |
 |---------------|----------------|----------------|-----------------|-----------------|
 |4WS(5CPU cycle)|120< HCLK <= 150|96 < HCLK <= 120|88 < HCLK <= 110 |80 < HCLK <= 100 |
 |---------------|----------------|----------------|-----------------|-----------------|
 |5WS(6CPU cycle)|150< HCLK <= 168|120< HCLK <= 144|110 < HCLK <= 132|100 < HCLK <= 120|
 |---------------|----------------|----------------|-----------------|-----------------|
 |6WS(7CPU cycle)|      NA        |144< HCLK <= 168|132 < HCLK <= 154|120 < HCLK <= 140|
 |---------------|----------------|----------------|-----------------|-----------------|
 |7WS(8CPU cycle)|      NA        |      NA        |154 < HCLK <= 168|140 < HCLK <= 160|
 +---------------|----------------|----------------|-----------------|-----------------+
 */

void unlockMFLASH(void)
{
	/// unlock the flash memory access
	if((FLASH->CR & FLASH_CR_LOCK) != RESET)
	{
		// authorize the FLASH registers access
		FLASH->KEYR = FLASH_KEY1;
		FLASH->KEYR = FLASH_KEY2;
	}
}

void lockMFLASH(void)
{
	/// lock access to flash memory
	FLASH->CR |= FLASH_CR_LOCK;
}

uint32_t eraseSectorMFLASH(uint32_t FLASH_Sector, uint8_t VoltageRange)
{
	// erase given flash memory sector
	uint32_t tmp_psize = 0x0;
	uint32_t status = FLASH_COMPLETE;

	if(VoltageRange == VoltageRange_1)
	{
		tmp_psize = FLASH_PSIZE_BYTE;
	}
	else if(VoltageRange == VoltageRange_2)
	{
		tmp_psize = FLASH_PSIZE_HALF_WORD;
	}
	else if(VoltageRange == VoltageRange_3)
	{
		tmp_psize = FLASH_PSIZE_WORD;
	}
	else
	{
		tmp_psize = FLASH_PSIZE_DOUBLE_WORD;
	}


	while((status = getStatusMFLASH()) == FLASH_BUSY);

	if(status == FLASH_COMPLETE)
	{
		/* if the previous operation is completed, proceed to erase the sector */
		FLASH->CR &= CR_PSIZE_MASK;
		FLASH->CR |= tmp_psize;
		FLASH->CR &= SECTOR_MASK;
		FLASH->CR |= FLASH_CR_SER | FLASH_Sector;
		FLASH->CR |= FLASH_CR_STRT;

		/* Wait for last operation to be completed */
		while((status = getStatusMFLASH()) == FLASH_BUSY);

		/* if the erase operation is completed, disable the SER Bit */
		FLASH->CR &= (~FLASH_CR_SER);
		FLASH->CR &= SECTOR_MASK;
	}
	/* Return the Erase Status */
	return status;
}

void writeByteMFLASH(uint32_t Address, uint8_t Data)
{
	// write single byte to FM, YOU must unlock the flash memory first!
	uint32_t stat;

	while((stat = getStatusMFLASH()) == FLASH_BUSY);						// wait for last operation to be completed

	if(stat == (FLASH_COMPLETE))
	{
		/* if the previous operation is completed, proceed to program the new data */
		FLASH->CR &= CR_PSIZE_MASK;
		FLASH->CR |= FLASH_PSIZE_BYTE;
		FLASH->CR |= FLASH_CR_PG;

		*(__IO uint8_t*)Address = Data;

		while((stat = getStatusMFLASH()) == FLASH_BUSY);					// wait for last operation to be completed

		FLASH->CR &= (~FLASH_CR_PG);									/* if the program operation is completed, disable the PG Bit */
	}
}

void writeWordMFLASH(uint32_t Address, uint32_t Data)
{
	// write single byte to FM, YOU must unlock the flash memory first!
	uint32_t stat;

	while((stat = getStatusMFLASH()) == FLASH_BUSY);						// wait for last operation to be completed

	if(stat == (FLASH_COMPLETE))
	{
		/* if the previous operation is completed, proceed to program the new data */
		FLASH->CR &= CR_PSIZE_MASK;
		FLASH->CR |= FLASH_PSIZE_WORD;
		FLASH->CR |= FLASH_CR_PG;

		*(__IO uint32_t*)Address = Data;

		while((stat = getStatusMFLASH()) == FLASH_BUSY);					// wait for last operation to be completed

		FLASH->CR &= (~FLASH_CR_PG);									/* if the program operation is completed, disable the PG Bit */
	}
}

uint32_t getStatusMFLASH(void)
{
	// get status of flash memory controller
	uint32_t flashstatus = FLASH_COMPLETE;

	if((FLASH->SR & FLASH_FLAG_BSY) == FLASH_FLAG_BSY)
	{
		flashstatus = FLASH_BUSY;
	}
	else
	{
		if((FLASH->SR & FLASH_FLAG_WRPERR) != (uint32_t)0x00)
		{
			flashstatus = FLASH_ERROR_WRP;
		}
		else
		{
			//if((FLASH->SR & FLASH_FLAG_RDERR) != (uint32_t)0x00)
			//{
			//flashstatus = FLASH_ERROR_RD;
			//}
			//else
			//{
			if((FLASH->SR & (uint32_t)0xEF) != (uint32_t)0x00)
			{
				flashstatus = FLASH_ERROR_PROGRAM;
			}
			else
			{
				if((FLASH->SR & FLASH_FLAG_OPERR) != (uint32_t)0x00)
				{
					flashstatus = FLASH_ERROR_OPERATION;
				}
				else
				{
					flashstatus = FLASH_COMPLETE;
				}
			}
			//}
		}
	}
	/* Return the FLASH Status */
	return flashstatus;
}

uint32_t getICsumMFLASH(uint32_t sector_size, uint32_t sector_addr)
{
	// get the 32b check sum of the new or base image in the flash memory
	uint8_t * addr = (uint8_t *)sector_addr;
	uint32_t k;
	uint8_t n=0;
	uint32_t csum = 0;
	uint32_t t_csum;

	for(k=0; k<sector_size; k++)
	{
		t_csum = *addr;
		addr++;

		if(n == 0)
		{
			t_csum = (t_csum<<24);
		}
		else if(n == 1)
		{
			t_csum = (t_csum<<16);
		}
		else if(n == 2)
		{
			t_csum = (t_csum<<8);
		}
		csum ^= t_csum;

		n++;
		if(n == 4)
			n = 0;
	}
	return csum;
}

uint32_t readByteMFLASH(uint32_t dst_addr, uint8_t * num, uint8_t len)
{
	// read 8, 16 or 32 bit number to flash memory while MSB going on lower address in flash memory
	uint8_t * fm_addr = (uint8_t *)dst_addr;
	uint8_t k;

	for(k=0; k<len; k++,num++,fm_addr++)
	{
		*num = *fm_addr;
	}
	return (dst_addr + len);
}




void eraseMainFwMFLASH(void)
{
#ifdef SYSTEM_DEBUG
	printUART("-> MFLASH: erasing main firmware MCU flash sectors...\n");
#endif
	uint32_t tim = getSYSTIM();

	unlockMFLASH();															// unlock flash memory for write operations

	FLASH->SR = ((FLASH_SR_EOP)|(FLASH_SR_SOP)|(FLASH_SR_WRPERR)|(FLASH_SR_PGAERR)|(FLASH_SR_PGPERR)|(FLASH_SR_PGSERR));


	eraseSectorMFLASH(FLASH_Sector_4, VoltageRange_3);						// erase flash sector 4 [ 64 kB]
	eraseSectorMFLASH(FLASH_Sector_5, VoltageRange_3);						// erase flash sector 5 [128 kB]
	eraseSectorMFLASH(FLASH_Sector_6, VoltageRange_3);						// erase flash sector 6 [128 kB]
	eraseSectorMFLASH(FLASH_Sector_7, VoltageRange_3);						// erase flash sector 7 [128 kB]

	lockMFLASH();															// lock the flash memory

	delay_ms(MAIN_FW_SECTOR_ERASE_TIME);

#ifdef SYSTEM_DEBUG
	printUART("\t completed in [%d]ms [%x]\n",getSYSTIM() - tim,FLASH->SR);
#endif

}
