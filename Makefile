# Put your stlink folder here so make burn will work.
STM32CUBEPROG:= /home/vdm/STMicroelectronics/STM32Cube/STM32CubeProgrammer/bin/STM32_Programmer.sh -vb 1 -q -c port=SWD 

# Binaries will be generated with this name (.elf, .bin, .hex, etc)
PROJ_NAME=main

# Put your source files here (or *.c, etc)
SRCS = main.c system_stm32f4xx.c   
SRCS += stm32f4xx_hal_msp.c stm32f4xx_it.c syscalls.c sysmem.c 


SRCS += usart.c misc.c delay.c config.c
#SRCS += midi.c
SRCS += terminal.c

SRCS += usb_device.c  
#SRCS += usbd_midi.c
#SRCS += usbd_midi_user.c
SRCS += parser.c
SRCS +=spi.c
SRCS +=timer.c
SRCS +=usbd_cdc.c
SRCS += usbd_cdc_if.c 
SRCS += usbd_conf.c 
SRCS += usbd_desc.c
SRCS += usb/Core/Src/usbd_core.c
SRCS += usb/Core/Src/usbd_ioreq.c
SRCS += usb/Core/Src/usbd_ctlreq.c
#SRCS += usb/Class/CDC/Src/usbd_cdc.c
#SRCS += usb/usb.c
SRCS += usb/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal.c 
SRCS += usb/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_cortex.c 
SRCS += usb/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_dma.c 
SRCS += usb/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_dma_ex.c 
SRCS += usb/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_exti.c 
SRCS += usb/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_flash.c 
SRCS += usb/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_flash_ex.c 
SRCS += usb/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_flash_ramfunc.c 
SRCS += usb/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_gpio.c 
SRCS += usb/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_pcd.c 
SRCS += usb/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_pcd_ex.c 
SRCS += usb/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_pwr.c 
SRCS += usb/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_pwr_ex.c 
SRCS += usb/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_rcc.c 
SRCS += usb/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_rcc_ex.c 
SRCS += usb/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_tim.c 
SRCS += usb/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_tim_ex.c 
SRCS += usb/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_ll_usb.c 


# Normally you shouldn't need to change anything below this line!
#######################################################################################
# GNUGCC = ../../../../../../gcc/gcc-arm-none-eabi-7-2018-q2-update/bin
GNUGCC =../gcc-arm-none-eabi-7-2018-q2-update/bin
CC = $(GNUGCC)/arm-none-eabi-gcc
OBJCOPY = $(GNUGCC)/arm-none-eabi-objcopy
SIZE =  $(GNUGCC)/arm-none-eabi-size

CFLAGS  = -g -O2 -Wfatal-errors -T stm32f446.ld 
CFLAGS += -mlittle-endian -mthumb -mcpu=cortex-m4 -mthumb-interwork
CFLAGS += -mfloat-abi=hard -mfpu=fpv4-sp-d16 -fsingle-precision-constant


# Include files from STM libraries
CFLAGS += -I./
CFLAGS += -I./sdk/
CFLAGS += -I./usb/Core/Inc/
# CFLAGS += -I./usb/Class/CDC/Inc/
CFLAGS += -I./usb/Drivers/STM32F4xx_HAL_Driver/Inc/

# add startup file to build
SRCS += ./sdk/startup_stm32f446xx.s
OBJS = $(SRCS:.c=.o)


.PHONY: proj

all:  $(PROJ_NAME).elf

$(PROJ_NAME).elf: $(SRCS)
	$(CC) $(CFLAGS) $^ -lm -lc -lnosys -o $@ 
	$(CC) $(CFLAGS) -S $< $^
	$(OBJCOPY) -O ihex $(PROJ_NAME).elf $(PROJ_NAME).hex
	$(OBJCOPY) -O binary $(PROJ_NAME).elf $(PROJ_NAME).bin
	$(SIZE) -B  $(PROJ_NAME).elf
	rm -rf *.s
	ls -l $(PROJ_NAME).bin
	# @./fwinfo

clean:
	rm -rf *.o $(PROJ_NAME).elf $(PROJ_NAME).hex $(PROJ_NAME).bin *.s
	ls

#upload: proj
#	$(STLINK)/st-flash write $(PROJ_NAME).bin 0x08000000

upload: $(PROJ_NAME).bin
	@$(STM32CUBEPROG) -w $(PROJ_NAME).bin  0x08000000 
	@sleep 1
	@$(STM32CUBEPROG) -hardRst 
	
upload-bootloader: ../pnp-bootloader/main.bin
	@$(STM32CUBEPROG) -w ../pnp-bootloader/main.bin  0x08000000 
	@sleep 1
	@$(STM32CUBEPROG) -hardRst 

