#ifndef __USART_H_
#define __USART_H_

#include <stdio.h>
#include <stdarg.h>
#include "stm32f4xx.h"
#include "misc.h"
#include "config.h"
#include "terminal.h"

#define USART3_BAUDRATE_921600		0x0000002D
#define USART3_BAUDRATE_115200		0x0000016C
#define USART_WAIT4TC_COUNT			100000
#define USART2_BAUDRATE_921600		0x0000002D
#define USART2_BAUDRATE_115200		0x0000016C
#define USART2_BAUDRATE_9600		0x00001117
#define USART2_BAUDRATE_19200		0x0000088B
#define USART2_BAUDRATE_57600		0x000002D9



#define USART1_BAUDRATE_921600		0x0000005B
#define USART1_BAUDRATE_115200		0x000002D9
#define USART1_BAUDRATE_9600		0x0000222e
#define USART1_BAUDRATE_19200		0x00001117
#define USART1_BAUDRATE_57600		0x000005B2


void putcharUSARTx(USART_TypeDef* usart,uint8_t data);




void initUSART3(uint32_t baudrate);
void putcharUSART3(uint8_t data);
void printUSART3(char * str, ... );
void sprintUSART3(uint8_t * str);
void USART3_IRQHandler(void);
void enIrqUSART3(void);
uint8_t getcharUSART3(void);

void enIrqUSART1(vcom* currentVCOM);
void disableIrqUSART1();
void initUSART1(uint32_t baudrate); 


void enIrqUSART2(vcom* currentVCOM); 
void disableIrqUSART2();
void initUSART2(uint32_t baudrate);
void putcharUSART2(uint8_t data);
void printUSART2(char * str, ... );
void sprintUSART2(uint8_t * str);
void clrRxBuffUSART2(void);
void clrTxBuffUSART2(void);
uint8_t txByteUSART2(uint8_t data);


uint32_t getRealBaud(uint8_t num,uint32_t baud);
#endif 
