#ifndef __DELAY_H_
#define __DELAY_H_

#include "stm32f4xx.h"


#define SYSTIM_TIMEOUT		0x00
#define SYSTIM_KEEP_ALIVE	0x01

void delay_ms(uint32_t ms);	 													
void delay_us(uint32_t ms);	

void 		initSYSTIM(void);
void 		deinitSYSTIM(void);
uint32_t 	getSYSTIM(void);
uint8_t 	chk4TimeoutSYSTIM(uint32_t btime, uint32_t period);		
uint32_t 	getElapsedTimeSYSTIM(uint32_t t_beg);

#endif 
