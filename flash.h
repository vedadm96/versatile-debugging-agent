#ifndef __FLASHMEMORY_H_
#define __FLASHMEMORY_H_

#include "stm32f4xx.h"
#include "config.h"
#include "delay.h"


#define VoltageRange_1        ((uint8_t)0x00)  /*!< Device operating range: 1.8V to 2.1V */
#define VoltageRange_2        ((uint8_t)0x01)  /*!<Device operating range: 2.1V to 2.7V */
#define VoltageRange_3        ((uint8_t)0x02)  /*!<Device operating range: 2.7V to 3.6V */
#define VoltageRange_4        ((uint8_t)0x03)  /*!<Device operating range: 2.7V to 3.6V + External Vpp */


#define FLASH_FLAG_EOP                 ((uint32_t)0x00000001)  /*!< FLASH End of Operation flag               */
#define FLASH_FLAG_OPERR               ((uint32_t)0x00000002)  /*!< FLASH operation Error flag                */
#define FLASH_FLAG_WRPERR              ((uint32_t)0x00000010)  /*!< FLASH Write protected error flag          */
#define FLASH_FLAG_PGAERR              ((uint32_t)0x00000020)  /*!< FLASH Programming Alignment error flag    */
#define FLASH_FLAG_PGPERR              ((uint32_t)0x00000040)  /*!< FLASH Programming Parallelism error flag  */
#define FLASH_FLAG_PGSERR              ((uint32_t)0x00000080)  /*!< FLASH Programming Sequence error flag     */
#define FLASH_FLAG_RDERR               ((uint32_t)0x00000100)  /*!< Read Protection error flag (PCROP)        */
#define FLASH_FLAG_BSY                 ((uint32_t)0x00010000)  /*!< FLASH Busy flag                           */ 
#define IS_FLASH_CLEAR_FLAG(FLAG) ((((FLAG) & (uint32_t)0xFFFFFE0C) == 0x00000000) && ((FLAG) != 0x00000000))
#define IS_FLASH_GET_FLAG(FLAG)  (((FLAG) == FLASH_FLAG_EOP)    || ((FLAG) == FLASH_FLAG_OPERR)  || \
                                  ((FLAG) == FLASH_FLAG_WRPERR) || ((FLAG) == FLASH_FLAG_PGAERR) || \
                                  ((FLAG) == FLASH_FLAG_PGPERR) || ((FLAG) == FLASH_FLAG_PGSERR) || \
                                  ((FLAG) == FLASH_FLAG_BSY)    || ((FLAG) == FLASH_FLAG_RDERR))

#define RDP_KEY                  ((uint16_t)0x00A5)
#define FLASH_KEY1               ((uint32_t)0x45670123)
#define FLASH_KEY2               ((uint32_t)0xCDEF89AB)
#define FLASH_OPT_KEY1           ((uint32_t)0x08192A3B)
#define FLASH_OPT_KEY2           ((uint32_t)0x4C5D6E7F)

#define FLASH_Sector_0     ((uint16_t)0x0000) /*!< Sector Number 0 */
#define FLASH_Sector_1     ((uint16_t)0x0008) /*!< Sector Number 1 */
#define FLASH_Sector_2     ((uint16_t)0x0010) /*!< Sector Number 2 */
#define FLASH_Sector_3     ((uint16_t)0x0018) /*!< Sector Number 3 */
#define FLASH_Sector_4     ((uint16_t)0x0020) /*!< Sector Number 4 */
#define FLASH_Sector_5     ((uint16_t)0x0028) /*!< Sector Number 5 */
#define FLASH_Sector_6     ((uint16_t)0x0030) /*!< Sector Number 6 */
#define FLASH_Sector_7     ((uint16_t)0x0038) /*!< Sector Number 7 */
#define FLASH_Sector_8     ((uint16_t)0x0040) /*!< Sector Number 8 */
#define FLASH_Sector_9     ((uint16_t)0x0048) /*!< Sector Number 9 */
#define FLASH_Sector_10    ((uint16_t)0x0050) /*!< Sector Number 10 */
#define FLASH_Sector_11    ((uint16_t)0x0058) /*!< Sector Number 11 */
#define IS_FLASH_SECTOR(SECTOR) (((SECTOR) == FLASH_Sector_0) || ((SECTOR) == FLASH_Sector_1) ||\
                                 ((SECTOR) == FLASH_Sector_2) || ((SECTOR) == FLASH_Sector_3) ||\
                                 ((SECTOR) == FLASH_Sector_4) || ((SECTOR) == FLASH_Sector_5) ||\
                                 ((SECTOR) == FLASH_Sector_6) || ((SECTOR) == FLASH_Sector_7) ||\
                                 ((SECTOR) == FLASH_Sector_8) || ((SECTOR) == FLASH_Sector_9) ||\
                                 ((SECTOR) == FLASH_Sector_10) || ((SECTOR) == FLASH_Sector_11))
#define IS_FLASH_ADDRESS(ADDRESS) ((((ADDRESS) >= 0x08000000) && ((ADDRESS) < 0x080FFFFF)) ||\
                                   (((ADDRESS) >= 0x1FFF7800) && ((ADDRESS) < 0x1FFF7A0F)))  

#define FLASH_PSIZE_BYTE           ((uint32_t)0x00000000)
#define FLASH_PSIZE_HALF_WORD      ((uint32_t)0x00000100)
#define FLASH_PSIZE_WORD           ((uint32_t)0x00000200)
#define FLASH_PSIZE_DOUBLE_WORD    ((uint32_t)0x00000300)
#define CR_PSIZE_MASK              ((uint32_t)0xFFFFFCFF)
#define SECTOR_MASK                ((uint32_t)0xFFFFFF07)

#define FLASH_BUSY					0x01
#define FLASH_ERROR_PGS				0x02
#define FLASH_ERROR_PGP				0x03
#define FLASH_ERROR_PGA				0x04
#define FLASH_ERROR_WRP				0x05
#define FLASH_ERROR_PROGRAM			0x06
#define FLASH_ERROR_OPERATION		0x07
#define FLASH_COMPLETE				0x08

#define BASE_IMAGE_SECTOR			0x00
#define NEW_IMAGE_SECTOR			0x01


///wwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwww
///----------------- USER DATA SPACE -----------------------------------
#define USER_DATA_SECTOR_ADDR			0x08004000
#define USER_DATA_SECTOR_SIZE			0x00004000

#define USER_DATA_SECTOR_LL_SIGN_ADDR	0x08004000
#define USER_DATA_SECTOR_LL_SIGN	    "iG2Bk"
#define USER_DATA_SECTOR_LL_SIGN_SIZE	0x05

#define USER_DATA_SECTOR_LL_NFFLAG_ADDR ((USER_DATA_SECTOR_LL_SIGN_ADDR) + (USER_DATA_SECTOR_LL_SIGN_SIZE))
#define USER_DATA_NO_NEW_FIRMWARE		0x00
#define USER_DATA_NEW_FIRMWARE			0xA5
#define USER_DATA_SECTOR_LL_NFFLAG_VAL	(USER_DATA_NO_NEW_FIRMWARE)
#define USER_DATA_SECTOR_LL_NFFLAG_SIZE	0x01

#define USER_DATA_SECTOR_FIRMWARE_SIZE_ADDR	((USER_DATA_SECTOR_LL_NFFLAG_ADDR) + (USER_DATA_SECTOR_LL_NFFLAG_SIZE))
#define USER_DATA_SECTOR_FIRMWARE_SIZE_VAL	0x00000000
#define USER_DATA_SECTOR_FIRMWARE_SIZE_SIZE	0x04
	
#define USER_DATA_APP_VALID_FLAG_ADDR	((USER_DATA_SECTOR_FIRMWARE_SIZE_ADDR) + (USER_DATA_SECTOR_FIRMWARE_SIZE_SIZE));
#define USER_DATA_VALID					0xA5
#define USER_DATA_NOT_VALID				0x00
#define USER_DATA_APP_VALID_FLAG_VAL	(USER_DATA_NOT_VALID)
#define USER_DATA_APP_VALID_FLAG_SIZE	0x01

#define USER_SPACE_PLAYER_ONE_ADDR		((USER_DATA_APP_VALID_FLAG_ADDR) + (USER_DATA_APP_VALID_FLAG_SIZE))
#define USER_SPACE_DATA_OK				0x00
#define USER_SPACE_DATA_MISSING			0x01
#define USER_DATA_SECTOR_CSUM_ADDR		0x08007FFC


///wwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwww
///----------------- FIRMWARE DATA SPACE -------------------------------
#define BASE_IMAGE_SECTOR_ADDR		0x08008000
#define NEW_IMAGE_SECTOR_ADDR		0x08080000



#define UPGRADE_SUCCESSFUL			0x00
#define UPGRADE_FAILED				0x01

#define USER_DATA_SECTOR_0	(FLASH_Sector_1)

#define BASE_IMAGE_SECTOR_0	(FLASH_Sector_2)
#define BASE_IMAGE_SECTOR_1	(FLASH_Sector_3)
#define BASE_IMAGE_SECTOR_2	(FLASH_Sector_4)
#define BASE_IMAGE_SECTOR_3	(FLASH_Sector_5)
#define BASE_IMAGE_SECTOR_4	(FLASH_Sector_6)
#define BASE_IMAGE_SECTOR_5	(FLASH_Sector_7)

#define NEW_IMAGE_SECTOR_0	(FLASH_Sector_8)
#define NEW_IMAGE_SECTOR_1	(FLASH_Sector_9)
#define NEW_IMAGE_SECTOR_2	(FLASH_Sector_10)
#define NEW_IMAGE_SECTOR_3	(FLASH_Sector_11)



#define USER_DATA_SECTOR_ERASE_TIME		2000								// time in ms
#define MAIN_FW_SECTOR_ERASE_TIME		2000					
#define FW_BLOCK_SIZE					128


void 		unlockMFLASH(void);
void 		lockMFLASH(void);

void 		writeByteMFLASH(uint32_t Address, uint8_t Data);
void 		writeWordMFLASH(uint32_t Address, uint32_t Data);
void 		eraseSectorsMFLASH(void);
uint32_t 	eraseSectorMFLASH(uint32_t FLASH_Sector, uint8_t VoltageRange);
uint32_t 	getStatusMFLASH(void);
uint32_t	getICsumMFLASH(uint32_t size, uint32_t sector);
void 		writeNewImageMFLASH(uint32_t size);

uint8_t 	chk4NewImageMFLASH(void);
uint32_t 	readByteMFLASH(uint32_t dst_addr, uint8_t * num, uint8_t len);
void 		setDef4BootMFLASH(void);
void 		clrNewFirmwareFlagMFLASH(void);

void 		eraseMainFwMFLASH(void);
#endif
