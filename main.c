#include "main.h"
#include "usbd_cdc_if.h"
#include "usb_device.h"
#include "delay.h"
#include "usart.h"
#include "stm32f446xx.h"
#include "terminal.h"
#include "parser.h"
#include "spi.h"
#include "timer.h"
static void MX_GPIO_Init(void);



vcom vcoms[MAX_VCOMS];
uart uarts[MAX_UARTS];
spi spis[MAX_SPIS];
timerU timers[MAX_TIMERS];
// extern struct vcom confTerm;
struct parser g_PARSER_INFO;
extern volatile uint8_t irqFlag;
uint8_t data_rec[6];
int16_t x,y,z;
int main(void)
{
	initUART(USART3_BAUDRATE_921600);
  initUSART2(USART2_BAUDRATE_921600);
  initUSART1(USART1_BAUDRATE_921600);
  initSPI1(SPI_BaudRatePrescaler_256);
  initSPI2(SPI_BaudRatePrescaler_256);
  initTIMER3();

	printUART("\nwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwww\n");
	printUART("w TEST app");
	printUART("\nwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwww\n");
  // printUART("\nHEXTEST <0x%xb>\n",'a');
  initVCOMS();
  initUARTS();
  initSPIS();
  initTIMERS();

  	HAL_Init();
	MX_GPIO_Init();
	MX_USB_DEVICE_Init();
      // sendLED();
  while(1){
      checkUsbOutput();
      for(uint8_t i=0;i<MAX_VCOMS;i++){
        if(vcoms[i].state!=IDLE_STATE && vcoms[i].state!=NOTCON_STATE){
          handleVCOMType(&vcoms[i]);
        }
      }

  }
}




static void MX_GPIO_Init(void)
{

  /* GPIO Ports Clock Enable */
  __HAL_RCC_GPIOH_CLK_ENABLE();
  __HAL_RCC_GPIOB_CLK_ENABLE();
  __HAL_RCC_GPIOA_CLK_ENABLE();

}

/* USER CODE BEGIN 4 */

/* USER CODE END 4 */

/**
  * @brief  This function is executed in case of error occurrence.
  * @retval None
  */
void Error_Handler(void)
{
  /* USER CODE BEGIN Error_Handler_Debug */
  /* User can add his own implementation to report the HAL error return state */

  /* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t *file, uint32_t line)
{ 
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
     tex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
