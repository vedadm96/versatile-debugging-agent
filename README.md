During the development of embedded systems, difficulties and issues are an in-
evitable part of the process. Finding the exact cause of the error requires copious
amounts of devices, debugger tools and time. Due to the nature of such errors, small
changes within the system can cause major turbulences.

Serial protocols such as USART / UART, SPI and I2C, timers that generate time
pulses, are specific to embedded systems. Their peripherals can be found in every mod-
ern microcontroller. Having an insight into the condition of the buses in real time is of
great importance and based on this idea, a hardware agent has been developed. The
hardware agent represents a union of tools that intends to cover the most important
needs during the development process.

The hardware agent uses the USB 2.0 protocol and behaves like a composite USB
device made up of communication classes. One of these communication classes(CDC)
presents itself as a configuration terminal through which the remaining classes can be
configured to take on the roles of SPI or USART/UART terminals.
