#include "parser.h"
#include "usbd_cdc_if.h"
#include "terminal.h"
#include "misc.h"
#include "spi.h"
	// [0][] - ADC data
	// [1][] - VI
	// [2][] - VQ
	// [3][] - Env2
	// [4][] - Env
	// [5][] - RMS
	// [6][] - EnvComp
	// [7][] - VIComp
	// [8][] - VQComp
	// [9][] - Ph
	// [10][] - FreqAudPlus



extern  struct parser g_PARSER_INFO;


uint8_t usart2Flag=0;

const char c_tx_dac_src[11][10] = {
	"Vout",
	"VIR",
	"VQR",
	"Env2R",
	"EnvR",
	"RMSR",
	"EnvCompR",
	"VICompR",
	"VQCompR",
	"PhR",
	"FrR",
};

const char c_rx_dac_src[11][10] = {
	"Vin1",
	"Vin2",
	"VI",
	"VQ",
	"LSB",
	"USB",
	"-",
	"-",
	"-",
	"-",
	"-",
};

// IMPORTANT: command strings must be sorted in ascending order
const PARSER_CMDS c_PARSER_CMDS[PARSER_CMD_COUNT] =
{
	{"?",					PARSER_CMD_HELP,				helpCmdPARSER,				0,		0,			PARSER_TYPE_INT}, // DONE 
	{"bind spi ",				PARSER_CMD_BIND_SPI,				bindSPI,				1,		2,			PARSER_TYPE_INT}, // DONE
	{"bind usart ",				PARSER_CMD_BIND_USART,				bindUSART,				1,		2,			PARSER_TYPE_INT}, // DONE
	{"help",				PARSER_CMD_HELP,				helpCmdPARSER,				0,		0,			PARSER_TYPE_INT}, // DONE
	{"get spi ",				PARSER_CMD_GET_SPI,					getSPI,				1,		2,			PARSER_TYPE_INT}, // DONE
	{"get status",				PARSER_CMD_GET_STATUS,					getStatus,				0,		0,			PARSER_TYPE_INT}, // DONE
	{"get usart ",				PARSER_CMD_GET_USART,				getUSART,				1,		2,			PARSER_TYPE_INT}, // DONE
	{"get vcom ",				PARSER_CMD_GET_VCOM,				getVCOM,				1,		2,			PARSER_TYPE_INT}, // DONE
	{"set baudrate ",				PARSER_CMD_SET_BAUDRATE,				setBaudrate,				1,		99999999,			PARSER_TYPE_INT}, // DONE
	{"set ccr1 ",				PARSER_CMD_SET_CCR1,				setCCR1,				0,		65536,			PARSER_TYPE_INT}, // DONE Maybe set channel, but too much selecting if I add that set timer -> set ch -> set pwm
	{"set ccr2 ",				PARSER_CMD_SET_CCR2,				setCCR2,				0,		65536,			PARSER_TYPE_INT}, // DONE
	{"set ccr3 ",				PARSER_CMD_SET_CCR3,				setCCR3,				0,		65536,			PARSER_TYPE_INT}, // DONE
	{"set ccr4 ",				PARSER_CMD_SET_CCR4,				setCCR4,				0,		65536,			PARSER_TYPE_INT}, // DONE 
	{"set cpha ",				PARSER_CMD_SET_CPHA,				setCpha,				0,		1,			PARSER_TYPE_INT}, // DONE
    {"set cpol ",				PARSER_CMD_SET_CPOL,				setCpol,				0,		1,			PARSER_TYPE_INT}, // DONE
	{"set period ",				PARSER_CMD_SET_PERIOD,				setPeriod,				1,		1000,			PARSER_TYPE_INT}, // DONE
	{"set prescaler ",				PARSER_CMD_SET_PRESCALER,				setPrescaler,				1,		256,			PARSER_TYPE_INT}, // DONE
	{"set spi ",				PARSER_CMD_SET_SPI,				setSPI,				1,		2,			PARSER_TYPE_INT}, // DONE
	{"set timer ",				PARSER_CMD_SET_TIMER,				setTimer,				3,		3,			PARSER_TYPE_INT}, // DONE
	{"set usart ",				PARSER_CMD_SET_USART,				setUSART,				1,		2,			PARSER_TYPE_INT}, // DONE
	{"set vcom ",				PARSER_CMD_SET_VCOM,				setVCOM,				1,		2,			PARSER_TYPE_INT}, // DONE
	{"unbind spi ",				PARSER_CMD_UNBIND_SPI,				unbindSPI,				1,		2,			PARSER_TYPE_INT}, // DONE
	{"unbind usart ",				PARSER_CMD_UNBIND_USART,				unbindUSART,				1,		2,			PARSER_TYPE_INT} // DONE
};

volatile uint8_t g_parser_type[6] = {0};
const uint32_t g_parser_digits[10] = {1000000000, 100000000, 10000000, 1000000, 100000, 10000, 1000, 100, 10, 1};
const uint32_t g_parser_type_digit[8] = {0x10000000, 0x01000000, 0x00100000, 0x00010000, 0x00001000, 0x00000100, 0x00000010, 0x00000001};
extern struct parser g_PARSER_INFO;
extern vcom vcoms[MAX_VCOMS];
extern uart uarts[MAX_UARTS];
extern spi spis[MAX_SPIS];
extern timerU timers[MAX_TIMERS];
vcom* selectedVcom=NULL;
uart* selectedUart=NULL;
spi* selectedSpi=NULL;
timerU* selectedTimer=NULL;






void addParserUARTBuffer(char* buff,uint16_t len){ 
    for(int16_t i=0;i<len;i++){ 
            g_PARSER_INFO.uart_buff[g_PARSER_INFO.uart_widx++]=buff[i]; 
            //  CDC_Transmit_FS(buff,len);
    }
}



void initPARSER(void)
{
	selectedUart=NULL;
	selectedVcom=NULL;
	selectedSpi=NULL;
	selectedTimer=NULL;
	uint16_t k, n;
	g_PARSER_INFO.get_cmd_bidx = 0;
	g_PARSER_INFO.get_cmd_eidx = 0;
	g_PARSER_INFO.set_cmd_bidx = 0;
	g_PARSER_INFO.set_cmd_eidx = 0;
	g_PARSER_INFO.update_flag = 0;
	g_PARSER_INFO.skip_str_parsing = 0;
	
	// find first get cmd
	for(k=0;k<(PARSER_CMD_COUNT);k++)
	{
		if(c_PARSER_CMDS[k].cmd[0] == 'g')
		{
			g_PARSER_INFO.get_cmd_bidx = k;
			break;
		}
	}
	
	for(;k<(PARSER_CMD_COUNT);k++)
	{
		if(c_PARSER_CMDS[k].cmd[0] != 'g')
		{
			g_PARSER_INFO.get_cmd_eidx = k;
			break;
		}
	}
	
	for(;k<(PARSER_CMD_COUNT);k++)
	{
		if(c_PARSER_CMDS[k].cmd[0] == 's')
		{
			g_PARSER_INFO.set_cmd_bidx = k;
			break;
		}
	}
	
	g_PARSER_INFO.set_cmd_eidx = (PARSER_CMD_COUNT); 
	for(;k<(PARSER_CMD_COUNT);k++)
	{
		if(c_PARSER_CMDS[k].cmd[0] != 's')
		{
			g_PARSER_INFO.set_cmd_eidx = k;
			break;
		}
	}
	
	
	g_PARSER_INFO.uart_widx = 0;
	g_PARSER_INFO.uart_ridx = (UART_PARSER_BUFFER_MASK);
	
	g_PARSER_INFO.widx = 0;
	g_PARSER_INFO.ridx = (PARSER_BUFFER_MASK); 
	
	g_PARSER_INFO.tab_cnt = 0;
	
	
	g_PARSER_INFO.exe_cmd_ridx = (PARSER_EXE_CMD_LIST_SIZE) - 1;
	g_PARSER_INFO.exe_cmd_widx = 0;
	g_PARSER_INFO.exe_cmd_pidx = 0;
	for(k=0;k<(PARSER_EXE_CMD_LIST_SIZE);k++)
	{
		g_PARSER_INFO.exe_cmd_list[k][0] = 0;
	}
	g_PARSER_INFO.tbuff = (uint8_t *)g_PARSER_INFO.exe_cmd_list[0];
	clrBuffPARSER();
	g_PARSER_INFO.spec_char = 0;
	g_PARSER_INFO.exe_cmd_from_list = 0;
	
	// printUART("\n\nwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwww\n");
	// printUART("w SSBMOD %s build %x                                                                   w");
	// printUART("\nwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwww");
	// while(CDC_Transmit_FS("\033[33;1m------------------------\033[0m\r\nConfiguration terminal:\r\n\033[33;1m------------------------\033[0m\r\n",100,0)!=USBD_OK){};

	printUsb("\033[33;1m------------------------\033[0m\r\nConfiguration terminal:\r\n\033[33;1m------------------------\033[0m\r\n",VCOM0);
	printTerminalPrefixPARSER();						
}

void clrBuffPARSER(void)
{
	uint16_t k;
	g_PARSER_INFO.widx = 0;
	for(k=0;k<(PARSER_BUFFER_SIZE);k++)
	{
		g_PARSER_INFO.tbuff[k] = 0x00;
		g_PARSER_INFO.buff[k] = 0x00;
	}
}

void chkPARSER(void)
{	
	uint16_t pidx = g_PARSER_INFO.uart_ridx;
	uint8_t flag = (PARSER_IDLE);												
	uint16_t k, n;
	uint8_t ch;	
	
	// ovaj dio se moze skipat
// 	if(g_PARSER_INFO.update_flag)
// 	{// check if we should print updated value from the M7 core
// 		if(chk4TimeoutSYSTIM(g_PARSER_INFO.update_param_timer, 50) == (SYSTIM_TIMEOUT))
// 		{
// 			g_PARSER_INFO.update_flag = 0;
// 			if(g_RES_SHARING_INFO->m4data_update_req[g_PARSER_INFO.update_param_idx] == g_RES_SHARING_INFO->m7data_update_rep[g_PARSER_INFO.update_param_idx])
// 			{
// #ifdef SYSTEM_DEBUG				
// 				printUART("-> CM4: PAR[%xb] REQ[%xb] REP[%xb] C[%d] N[%d]\n",g_PARSER_INFO.update_param_idx,g_RES_SHARING_INFO->m4data_update_req[g_PARSER_INFO.update_param_idx],g_RES_SHARING_INFO->m7data_update_rep[g_PARSER_INFO.update_param_idx], g_RES_SHARING_INFO->cv_data[g_PARSER_INFO.update_param_idx],g_RES_SHARING_INFO->nv_data[g_PARSER_INFO.update_param_idx]);
// #endif				
// 				g_PARSER_INFO.skip_str_parsing = 1;
// 				g_PARSER_INFO.update_param_fnc();
// 				g_PARSER_INFO.skip_str_parsing = 0;
				
// 				printTerminalPrefixPARSER();
				
// 			}
// 			else
// 			{
// 				printUART("\nERROR: failed to update parameter");
// 				g_PARSER_INFO.update_param_fnc();
// 				printTerminalPrefixPARSER();
// 				g_RES_SHARING_INFO->m7data_update_rep[g_PARSER_INFO.update_param_idx] = g_RES_SHARING_INFO->m4data_update_req[g_PARSER_INFO.update_param_idx];
// 			}
			
// 		}
// 		else
// 		{
// 			return;
// 		}
// 	}
	
	//wwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwww
	// printing and data capturing part of the parser
	//--------------------------------------------------------------------------
	while(1)
	{
		pidx++;
		pidx &= (UART_PARSER_BUFFER_MASK);
		if(pidx == g_PARSER_INFO.uart_widx)
		{// no more characters in UART buffer
			break;
		}
		
		// printUART("CHAR %xb UW[%d] UR[%d]\n",g_PARSER_INFO.uart_buff[pidx],g_PARSER_INFO.uart_widx,g_PARSER_INFO.uart_ridx);
		if(g_PARSER_INFO.uart_buff[pidx] == 0x7F)
		{// delete character 
			
			g_PARSER_INFO.tab_cnt = 0;
			g_PARSER_INFO.spec_char = 0;
			
			if(g_PARSER_INFO.widx > 0)
			{
				// putcharUART('\b');
				// putcharUART(' ');
				// putcharUART('\b');
				// while(CDC_Transmit_FS("\b \b",3,0)!=USBD_OK){};
				printUsb("\b \b",VCOM0);
				g_PARSER_INFO.widx--;
			}
				
			g_PARSER_INFO.tbuff[g_PARSER_INFO.widx] = 0x00;
		}
		else if(((g_PARSER_INFO.uart_buff[pidx] >= 0x30)&&(g_PARSER_INFO.uart_buff[pidx] <= 0x39))|| 
		((g_PARSER_INFO.uart_buff[pidx] >= 0x41)&&(g_PARSER_INFO.uart_buff[pidx] <= 0x5A))|| 
		((g_PARSER_INFO.uart_buff[pidx] >= 0x61)&&(g_PARSER_INFO.uart_buff[pidx] <= 0x7A))||
		(g_PARSER_INFO.uart_buff[pidx] == '.')||(g_PARSER_INFO.uart_buff[pidx] == 0x20)||
		(g_PARSER_INFO.uart_buff[pidx] == 0x0D)||(g_PARSER_INFO.uart_buff[pidx] == 0x0A)||(g_PARSER_INFO.uart_buff[pidx] == '?'))
		{
			uint16_t kmax = 0;
			g_PARSER_INFO.tab_cnt = 0;
			
			if((g_PARSER_INFO.uart_buff[pidx] == 0x41)&&(g_PARSER_INFO.spec_char == 2))
			{// up arrow
				
				
				kmax = lenStrMISC((uint8_t *)g_PARSER_INFO.exe_cmd_list[g_PARSER_INFO.exe_cmd_pidx]);
				uint8_t exe_cmd_pidx = g_PARSER_INFO.exe_cmd_pidx;
				exe_cmd_pidx--;
				if(exe_cmd_pidx == 0xFF)
					exe_cmd_pidx = (PARSER_EXE_CMD_LIST_SIZE) - 1;
					
				if(g_PARSER_INFO.exe_cmd_list[exe_cmd_pidx][0] != 0)
				{// its not an empty command
					if(exe_cmd_pidx != g_PARSER_INFO.exe_cmd_widx)
					{
						g_PARSER_INFO.exe_cmd_pidx = exe_cmd_pidx;
					}
				}
				else
				{
					//printUART("up arrow empty pidx %d widx %d\n",exe_cmd_pidx,g_PARSER_INFO.exe_cmd_widx);	
				}
				
				// erase previous command
				for(k=0;k<kmax;k++)
				{
					// putcharUART('\b');
					// putcharUART(' ');
					// putcharUART('\b');
					// while(CDC_Transmit_FS("\b \b",3,0)!=USBD_OK){};
					putcharVcom('\b',VCOM0);
					putcharVcom(' ',VCOM0);
					putcharVcom('\b',VCOM0);

				}
				
				// print current command text & restore write index
				g_PARSER_INFO.widx = 0;
				for(k=0;k<(PARSER_BUFFER_SIZE);k++)
				{
					if(g_PARSER_INFO.exe_cmd_list[g_PARSER_INFO.exe_cmd_pidx][k] == 0)
					{
						break;
					}
					else
					{
						// putcharUART(g_PARSER_INFO.exe_cmd_list[g_PARSER_INFO.exe_cmd_pidx][k]);
						// while(CDC_Transmit_FS(g_PARSER_INFO.exe_cmd_list[g_PARSER_INFO.exe_cmd_pidx][k],1,0)!=USBD_OK){};
						putcharVcom(g_PARSER_INFO.exe_cmd_list[g_PARSER_INFO.exe_cmd_pidx][k],VCOM0);						
						g_PARSER_INFO.widx++;
					}
				}
				
				g_PARSER_INFO.tbuff = (uint8_t *)g_PARSER_INFO.exe_cmd_list[g_PARSER_INFO.exe_cmd_pidx];	
				//printUART("up arrow pidx %d widx %d\n",g_PARSER_INFO.exe_cmd_pidx,g_PARSER_INFO.exe_cmd_widx);	
			}
			else if((g_PARSER_INFO.uart_buff[pidx] == 0x42)&&(g_PARSER_INFO.spec_char == 2))
			{// down arrow
				kmax = lenStrMISC((uint8_t *)g_PARSER_INFO.exe_cmd_list[g_PARSER_INFO.exe_cmd_pidx]);
				
				g_PARSER_INFO.exe_cmd_pidx++;
				if(g_PARSER_INFO.exe_cmd_widx == ((PARSER_EXE_CMD_LIST_SIZE)-1))
				{
					if(g_PARSER_INFO.exe_cmd_pidx > g_PARSER_INFO.exe_cmd_widx)
					{
						g_PARSER_INFO.exe_cmd_pidx = g_PARSER_INFO.exe_cmd_widx;
					}
				}
				else
				{
					 
					if(g_PARSER_INFO.exe_cmd_pidx >= ((PARSER_EXE_CMD_LIST_SIZE)-1))
						g_PARSER_INFO.exe_cmd_pidx = 0;
						
					if(g_PARSER_INFO.exe_cmd_pidx > g_PARSER_INFO.exe_cmd_widx)
						g_PARSER_INFO.exe_cmd_pidx = g_PARSER_INFO.exe_cmd_widx;
				} 
				
				// erase previous command
				for(k=0;k<kmax;k++)
				{
					putcharVcom('\b',VCOM0);
					putcharVcom(' ',VCOM0);
					putcharVcom('\b',VCOM0);
				}
				
				// print current command text & restore write index
				g_PARSER_INFO.widx = 0;
				for(k=0;k<(PARSER_BUFFER_SIZE);k++)
				{
					if(g_PARSER_INFO.exe_cmd_list[g_PARSER_INFO.exe_cmd_pidx][k] == 0)
					{
						break;
					}
					else
					{
						// putcharUART(g_PARSER_INFO.exe_cmd_list[g_PARSER_INFO.exe_cmd_pidx][k]);
						putcharVcom(g_PARSER_INFO.exe_cmd_list[g_PARSER_INFO.exe_cmd_pidx][k],VCOM0);
						g_PARSER_INFO.widx++;
					}
				}
				g_PARSER_INFO.tbuff = (uint8_t *)g_PARSER_INFO.exe_cmd_list[g_PARSER_INFO.exe_cmd_pidx];
				//printUART("down arrow pidx %d widx %d\n",g_PARSER_INFO.exe_cmd_pidx,g_PARSER_INFO.exe_cmd_widx);	
			}
			else if(g_PARSER_INFO.uart_buff[pidx] == 0x0D)
			{
				flag |= (PARSER_CMD_CAPTURED);
			}
			else
			{
				// show typed char
				// putcharUART(g_PARSER_INFO.uart_buff[pidx]);
				// CDC_Transmit_FS(g_PARSER_INFO.uart_buff+pidx,1,0);
				if((g_PARSER_INFO.uart_buff[pidx]!=0x43 || g_PARSER_INFO.uart_buff[pidx]!=0x44) && g_PARSER_INFO.spec_char!=2)
					copyToOutputBuffer(&vcoms[0],g_PARSER_INFO.uart_buff+pidx,1);
			}
			
			
			
			if(g_PARSER_INFO.widx >= (PARSER_BUFFER_SIZE))
			{
				flag |= (PARSER_BUFFER_OVERFLOW);
			}
			else
			{
				if(g_PARSER_INFO.spec_char == 0)
				{
					// printUART("\nwidx[%d] pidx[%d] uart_buff[%c]",g_PARSER_INFO.widx,pidx,g_PARSER_INFO.uart_buff[pidx]);
					g_PARSER_INFO.tbuff[g_PARSER_INFO.widx] = g_PARSER_INFO.uart_buff[pidx];
					g_PARSER_INFO.widx++;
				}
				// 	printUART("\n tbuff[ ");
				// for(uint16_t i=0;i<g_PARSER_INFO.widx;i++){
				// 	printUART("%c ",g_PARSER_INFO.tbuff[i]);
				// } 
				// 	printUART("]\n");
			}
			g_PARSER_INFO.spec_char = 0;
			
		}
		else if((g_PARSER_INFO.uart_buff[pidx] == 0x1B)||(g_PARSER_INFO.uart_buff[pidx] == 0x5B))
		{// detection of prefix for up and down arrows
			g_PARSER_INFO.spec_char++;
		}
		else if(g_PARSER_INFO.uart_buff[pidx] == 0x09)
		{// tab character
			g_PARSER_INFO.tab_cnt++;
			g_PARSER_INFO.spec_char = 0; 
			if(g_PARSER_INFO.tab_cnt >= 2)
			{
				g_PARSER_INFO.tab_cnt = 2;
				// print all commands which have same prefix
				uint16_t m, p;
				uint8_t flag;
				uint8_t nflag = 1;
				uint8_t cmd_cnt = 0;
				g_PARSER_INFO.tbuff[g_PARSER_INFO.widx] = 0;
				//printUART("UART DATA [%d] %s\n",g_PARSER_INFO.widx,g_PARSER_INFO.tbuff);
				for(m=0;m<(PARSER_CMD_COUNT);m++)
				{
					flag = 1;
					for(p=0;p<g_PARSER_INFO.widx;p++)
					{
						if(c_PARSER_CMDS[m].cmd[p] == 0)
						{
							flag = 0;
							break;
						}
						else if(g_PARSER_INFO.tbuff[p] != c_PARSER_CMDS[m].cmd[p])
						{
							flag = 0;
							break;
						}
					}
					
					if(flag)
					{
						if(nflag)
						{
							nflag = 0;
							// putcharUART('\n');
							// putcharUART('\r');
							// while(CDC_Transmit_FS("\r\n",3,0)!=USBD_OK){};
							putcharVcom('\n',VCOM0);
							putcharVcom('\r',VCOM0);
						}
						uint8_t len = lenStrMISC((uint8_t *)c_PARSER_CMDS[m].cmd); 
							// while(CDC_Transmit_FS(c_PARSER_CMDS[m].cmd,128,0)!=USBD_OK){};
							copyToOutputBuffer(&vcoms[0],c_PARSER_CMDS[m].cmd,128);
						// printUART("%s",c_PARSER_CMDS[m].cmd);
						// for(p=0;p<((PARSER_TABED_CMD_LEN) - len);p++)
						// 	putcharUART(' '); 
							// while(CDC_Transmit_FS("                     ",22,0)!=USBD_OK){};
							printUsb("                      ",VCOM0);
							
						cmd_cnt++;
						if(cmd_cnt >= 4)
						{
							cmd_cnt = 0;
							// while(CDC_Transmit_FS("\r\n",3,0)!=USBD_OK){};
							putcharVcom('\n',VCOM0);
							putcharVcom('\r',VCOM0);
						}
					}
				}
				
				if(nflag == 0)
				{// print user typed text again
					printTerminalPrefixPARSER();
					// printUART("%s",g_PARSER_INFO.tbuff); 
					// while(CDC_Transmit_FS(g_PARSER_INFO.tbuff,g_PARSER_INFO.widx,0)!=USBD_OK){};
					copyToOutputBuffer(&vcoms[0],g_PARSER_INFO.tbuff,g_PARSER_INFO.widx);
				}
			}
		}
		
		
		g_PARSER_INFO.uart_ridx = pidx;
		
		if(flag != (PARSER_IDLE))												// buffer overflow detected or string captured
			break;
	}
	
	
	//wwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwww
	// data analysis -> actual cmd parsing 
	//--------------------------------------------------------------------------
	if(flag != (PARSER_IDLE))
	{	
		if(g_PARSER_INFO.widx < 2)
		{
			g_PARSER_INFO.tbuff[g_PARSER_INFO.widx-1] = 0x00;
			//printCmdNotFoundPARSER();
			printTerminalPrefixPARSER();
			clrBuffPARSER();
			return;
		}
		
			
		g_PARSER_INFO.tbuff[g_PARSER_INFO.widx-1] = 0x00;
		g_PARSER_INFO.widx--;
		
		
		// remove successive space characters
		uint16_t twidx = g_PARSER_INFO.widx;
		uint8_t sflag = 0;
		uint8_t fsflag = 0;
		uint16_t widx = 0;
		for(k=0;k<g_PARSER_INFO.widx;k++)
		{
			if(g_PARSER_INFO.tbuff[k] == 0x20)
			{
				if(sflag == 0)
				{
					sflag = 1;
					if(k != 0)
					{
						g_PARSER_INFO.buff[widx++] = g_PARSER_INFO.tbuff[k];
					}
				}
			}
			else
			{
				sflag = 0;
				g_PARSER_INFO.buff[widx++] = g_PARSER_INFO.tbuff[k];
			}
			
			//printUART("CHAR[%xb] F[%d] k[%d] w[%d]\n",g_PARSER_INFO.tbuff[k],sflag,k,widx);
		}
		if(widx != g_PARSER_INFO.widx)
			g_PARSER_INFO.widx = widx;
		
		
		// remove last space in the command
		if(g_PARSER_INFO.buff[g_PARSER_INFO.widx-1] == 0x20)
			g_PARSER_INFO.widx--;
		
		uint16_t tidx, bidx = 0;
		uint16_t eidx = (PARSER_CMD_COUNT)-1;
		uint16_t stage;	
		uint8_t prefix_space_offset = 0;
		uint8_t prefix_space_flag = 0;
		for(k=0;k<g_PARSER_INFO.widx;k++)										
		{// go through all characters of the input string to check if there is any command prefix
					
			stage = 0;
			tidx = eidx;
			for(n=bidx;n<=tidx;n++)
			{
				// convert
				ch = g_PARSER_INFO.buff[k];
				if((ch >= 0x41)&&(ch <= 0x5A))
				{
					ch = ch + 0x20;
				}
				
				if(stage == 0)
				{
					if(ch == c_PARSER_CMDS[n].cmd[k])
					{
						
						bidx = n;
						stage = 1;
					}
				}
				else
				{
					if(ch != c_PARSER_CMDS[n].cmd[k])
					{
						eidx = n - 1;
						break;
					}
					else
					{
						eidx = n;
					}
				}
			}
			
			if(stage == 0)
			{// string not found
				break;
			}
		}
		
		printUART("CMD IDXb:%d b[%d] e[%d] widx[%d] cmd_len[%d] stage %d\n",g_PARSER_INFO.cmd_idx,bidx,eidx,g_PARSER_INFO.widx,g_PARSER_INFO.cmd_len, stage);
		// in case there are multiple commands having same prefix remove those which differ in size
		if(stage)
		{
			for(k=bidx;k<=eidx;k++)
			{
				if(g_PARSER_INFO.widx == lenStrMISC((uint8_t *)c_PARSER_CMDS[k].cmd))
				{
					bidx = k;
					eidx = k;
					k = g_PARSER_INFO.widx;
					//g_PARSER_INFO.cmd_idx = k;
					//g_PARSER_INFO.cmd_len = g_PARSER_INFO.widx;
					break;
				}
			}
		}
		
		printUART("CMD IDXa:%d b[%d] e[%d] widx[%d] cmd_len[%d]\n",g_PARSER_INFO.cmd_idx,bidx,eidx,g_PARSER_INFO.widx,g_PARSER_INFO.cmd_len);
		uint8_t flag = 0;
		if(bidx == eidx)
		{// check if start and stop indices are the same 
			g_PARSER_INFO.cmd_len = lenStrMISC((uint8_t *)(c_PARSER_CMDS[bidx].cmd));
			g_PARSER_INFO.cmd_idx = bidx;
			//g_PARSER_INFO.cmd_print_idx = bidx;
			
			if(c_PARSER_CMDS[bidx].cmd[g_PARSER_INFO.cmd_len-1] == ' ')
			{// selected command has input argument,
				
				if(g_PARSER_INFO.buff[g_PARSER_INFO.cmd_len] == '?')
				{// display command range
					printCmdRangePRASER();
					flag = 1;
				}
				else
				{ 
				
					// if((g_MODE_INFO.state == (MODE_STATE_RX))||(c_PARSER_CMDS[g_PARSER_INFO.cmd_idx].fnc == setModePARSER))
					// {
						c_PARSER_CMDS[g_PARSER_INFO.cmd_idx].fnc();
					// }
					// else
					// {
						// printUART("\nerror: can not set parameter please switch to Rx Mode");
					// }
				}
				
				flag = 1;
			}
			else
			{// potentially selected command do not have input argument, check if the lengths match
				if((k == g_PARSER_INFO.cmd_len)&&(g_PARSER_INFO.widx == g_PARSER_INFO.cmd_len))
				{// lengths are equal and the strings are identical
					c_PARSER_CMDS[g_PARSER_INFO.cmd_idx].fnc();
					flag = 1;	
				}
				else
				{
					
				}
			}
		}
		
		
		if(flag == 0)
		{
			printCmdNotFoundPARSER();
			g_PARSER_INFO.update_flag = 0;
		}
		
		if(g_PARSER_INFO.update_flag == 0)
		{
			printTerminalPrefixPARSER();
		}
		
		
		if(g_PARSER_INFO.exe_cmd_pidx != g_PARSER_INFO.exe_cmd_widx)
		{
			for(k=0;k<(PARSER_BUFFER_SIZE);k++)
			{
				g_PARSER_INFO.exe_cmd_list[g_PARSER_INFO.exe_cmd_widx][k] = g_PARSER_INFO.exe_cmd_list[g_PARSER_INFO.exe_cmd_pidx][k];
			}
		}
		
		g_PARSER_INFO.exe_cmd_widx++;
		if(g_PARSER_INFO.exe_cmd_widx >= (PARSER_EXE_CMD_LIST_SIZE))
			g_PARSER_INFO.exe_cmd_widx = 0;
		
		g_PARSER_INFO.exe_cmd_pidx = g_PARSER_INFO.exe_cmd_widx;
		g_PARSER_INFO.tbuff = (uint8_t *)g_PARSER_INFO.exe_cmd_list[g_PARSER_INFO.exe_cmd_widx];
		//printUART("cmd_list idx %d\n",g_PARSER_INFO.exe_cmd_widx);	
		clrBuffPARSER();
	}
	
}

void printCmdRangePRASER(void)
{
	uint8_t k;
	printUART("CMD ID: %xb\n",c_PARSER_CMDS[g_PARSER_INFO.cmd_idx].cmd_id);
	if((c_PARSER_CMDS[g_PARSER_INFO.cmd_idx].cmd_id == (PARSER_CMD_SET_ALGORXDAC1SRC))||(c_PARSER_CMDS[g_PARSER_INFO.cmd_idx].cmd_id == (PARSER_CMD_SET_ALGORXDAC2SRC)))
	{
		printUART("\ninput range:");
		for(k=0;k<11;k++)
		{
			if(k < 9)
			{
				printUART("\n\t  %d - %s", k+1,c_rx_dac_src[k]);
			}
			else
			{
				printUART("\n\t %d - %s", k+1,c_rx_dac_src[k]);
			}
		}
	}
	else if((c_PARSER_CMDS[g_PARSER_INFO.cmd_idx].cmd_id == (PARSER_CMD_SET_ALGOTXDAC1SRC))||(c_PARSER_CMDS[g_PARSER_INFO.cmd_idx].cmd_id == (PARSER_CMD_SET_ALGOTXDAC2SRC)))
	{
		printUART("\ninput range:");
		for(k=0;k<11;k++)
		{
			if(k < 9)
			{
				printUART("\n\t  %d - %s", k+1,c_tx_dac_src[k]);
			}
			else
			{
				printUART("\n\t %d - %s", k+1,c_tx_dac_src[k]);
			}
		}
	}
	else
	{
		printUART("\ninput range: %d - %d", c_PARSER_CMDS[g_PARSER_INFO.cmd_idx].range_min,c_PARSER_CMDS[g_PARSER_INFO.cmd_idx].range_max);
	}
}

void printTerminalPrefixPARSER(void)
{
	// printUART("\nuser@ssbmod$: ");
	// while(CDC_Transmit_FS("\r\nuser$:",9,0)!=USBD_OK){
	// 	printUART("\nUSBD not ready");
	// } 
		// CDC_Transmit_FS("\r\nuser$:",9,0);
		printUsb("\n\033[32;1muser$\033[0m:",VCOM0);
}

// void printMsgFromIndPARSER(uint8_t idx)
// {
// 	printUART("\nSW%d \"%dm\" pressed", idx+1, c_HAM_BANDS[idx].length);
// 	printTerminalPrefixPARSER();
// 	clrBuffPARSER();
// }

void printIndMsgPARSER(uint8_t * data, uint8_t type)
{
	if(type == 0)
	{
		printUART("\nMODBUS Tx: %s", data);
	}
	else
	{
		printUART("\nMODBUS Rx: %s", data);
	}	
		
	printTerminalPrefixPARSER();
	clrBuffPARSER();
}

void printInvalidRangePARSER(void)
{
	printUsb("\nerror: invalid range or agrument count",VCOM0);
}

void printCmdNotFoundPARSER(void)
{
	uint8_t ch;
	uint16_t k, len = lenStrMISC((uint8_t *)g_PARSER_INFO.buff);
	// putcharUART('\r');
	// putcharUART('\n');
	// for(k=0;k<len;k++)
	// {
	// 	ch = g_PARSER_INFO.buff[k];
	// 	if((ch >= 0x61)&&(ch <= 0x7A))
	// 	{
	// 		ch = ch-0x20;
	// 	}
	// 	putcharUART(ch);
	// }
	// printUART(": command not found");

	// while(CDC_Transmit_FS("\r\n",2,0)!=USBD_OK){};
	printUsb("\r\n",VCOM0);
	// while(CDC_Transmit_FS(g_PARSER_INFO.buff,len,0)!=USBD_OK){};
	copyToOutputBuffer(&vcoms[0],g_PARSER_INFO.buff,len);
	// while(CDC_Transmit_FS(": command not found\r",20,0)!=USBD_OK){};
	printUsb(": command not found\r",VCOM0);
}

void printCmdRangePARSER(void)
{
	uint8_t ch;
	uint8_t flag = 0;
	uint16_t k, len = lenStrMISC((uint8_t *)g_PARSER_INFO.buff);
	putcharUART('\r');
	putcharUART('\n');
	for(k=0;k<len;k++)
	{
		ch = g_PARSER_INFO.buff[k];
		if((ch >= 0x61)&&(ch <= 0x7A))
		{
			ch = ch-0x20;
		}
		else if(ch == 0x20)
		{
			flag++;
			if(flag == 2)
			{
				g_PARSER_INFO.buff[k] = 0;
				break;
			}
		}
		putcharUART(ch);
	}
	printUART(" [%d - %d]",c_PARSER_CMDS[g_PARSER_INFO.cmd_idx].range_min,c_PARSER_CMDS[g_PARSER_INFO.cmd_idx].range_max);
}

void helpCmdPARSER(void)
{//

	printUsb("\033[34;1m\nset/get vcom x\033[0m\n",VCOM0);
	printUsb(" Selects VCOMx x=1,2\n",VCOM0);
	
	printUsb("\033[34;1mset/get spi x\033[0m\n",VCOM0);
	printUsb(" Selects SPIx x=1,2\n",VCOM0);

	printUsb("\033[34;1mset/get usart x\033[0m\n",VCOM0);
	printUsb(" Selects USARTx x=1,2\n",VCOM0);
	
	printUsb("\033[34;1mset timer x\033[0m\n",VCOM0);
	printUsb(" Selects TIMERx x=3\n",VCOM0);

	printUsb("\033[34;1mset ccrx y\033[0m\n",VCOM0);
	printUsb(" Sets the CCRX for the currently selected TIMER y=0-65536\n",VCOM0);
	
	printUsb("\033[34;1mset period x\033[0m\n",VCOM0);
	printUsb(" Sets the period for the currently selected TIMER y=1-1000ms\n",VCOM0);
	
	printUsb("\033[34;1mset baudrate x\033[0m\n",VCOM0);
	printUsb(" Sets the baudrate for the currently selected USART x=9600,19200,57600,115200,921600\n",VCOM0);
	
	printUsb("\033[34;1mset prescaler x\033[0m\n",VCOM0);
	printUsb(" Sets the prescaler for the currently selected SPI x=2,4,8,16,32,64,128,256\n",VCOM0);
	
	printUsb("\033[34;1mset cpha x\033[0m\n",VCOM0);
	printUsb(" Sets the CPHA for the currently selected SPI x=0,1\n",VCOM0);

	printUsb("\033[34;1mset cpol x\033[0m\n",VCOM0);
	printUsb(" Sets the CPOL for the currently selected SPI x=0,1\n",VCOM0);

	printUsb("\033[34;1mget status\033[0m\n",VCOM0);
	printUsb(" Shows data for the currently selected VCOM,SPI,USART\n",VCOM0);
	
	printUsb("\033[34;1mbind spi x\033[0m\n",VCOM0);
	printUsb(" Binds the selected SPI to the currently selected VCOM x=1\n",VCOM0);
	
	printUsb("\033[34;1mbind usart x\033[0m\n",VCOM0);
	printUsb(" Binds the selected USART to the currently selected VCOM x=1,2\n",VCOM0);
	
}

uint8_t getArgsPARSER(uint8_t pars_type, uint8_t num_args)
{
	uint16_t k = g_PARSER_INFO.cmd_len;
	uint16_t kmax = g_PARSER_INFO.widx + 1;
	uint8_t flag = (PARSER_FIND_FIRST_SPACE);
	
	
	g_PARSER_INFO.arg_int[0] = 0;		
	g_PARSER_INFO.arg_frac[0] = 0;		
	uint8_t dot_flag = 0;
	uint8_t idx = 0;
	for(k=0;k<16;k++)
	{
		g_PARSER_INFO.str_before_dot[k] = 0;
		g_PARSER_INFO.str_after_dot[k] = 0;
	}

	k = 0;	
	while(1)
	{
		//printUART("k[%d] I[%c]=[%xb]\n",k,g_PARSER_INFO.buff[k],g_PARSER_INFO.buff[k]);
		switch(flag)
		{
			case(PARSER_FIND_FIRST_SPACE):
			{
				if(g_PARSER_INFO.buff[k] == ' ')
				{
					flag = (PARSER_FIND_FIRST_NONSPACE);
				}
				k++;
				break;
			}
			case(PARSER_FIND_FIRST_NONSPACE):
			{
				if(g_PARSER_INFO.buff[k] != ' ')
				{
					flag = (PARSER_FIND_SECOND_SPACE);
				}
				k++;
				
				break;
			}
			case(PARSER_FIND_SECOND_SPACE):
			{
				if(g_PARSER_INFO.buff[k] == ' ')
				{
					flag = (PARSER_SEARCH4DATA);
				}
				k++;
				break;
			}
			case(PARSER_SEARCH4DATA):
			{
				if(g_PARSER_INFO.buff[k] != ' ')
				{
					if(g_PARSER_INFO.buff[k] == '?')
					{
						printCmdRangePARSER();
						return (PARSER_RANGE_HELP);
					}
					flag = (PARSER_CAPTURE_DATA);
				}
				else
				{
					k++;
				}
				break;
			}
			case(PARSER_CAPTURE_DATA):
			{
				if((g_PARSER_INFO.buff[k] >= 0x30)&&(g_PARSER_INFO.buff[k] <= 0x39))
				{
					if(dot_flag == 0)
					{
						
						g_PARSER_INFO.str_before_dot[idx++] = g_PARSER_INFO.buff[k];
					}
					else
					{
						g_PARSER_INFO.str_after_dot[idx++] = g_PARSER_INFO.buff[k];
					}
					
					k++;
					if(k >= kmax)
					{
						uint8_t rval = getNum4PARSER();
						return rval;
					}
				}
				else if(g_PARSER_INFO.buff[k] == '.')
				{
					idx = 0;
					dot_flag = 1;
					k++;
					if(k >= kmax)
					{
						uint8_t rval = getNum4PARSER();
						return rval;
					}
				}
				else if((g_PARSER_INFO.buff[k] == 0x00)||(g_PARSER_INFO.buff[k] == 0x0D)||(g_PARSER_INFO.buff[k] == 0x20)||(g_PARSER_INFO.buff[k] == 0x0A))
				{
					uint8_t rval = getNum4PARSER();
					return rval;
				}
				else
				{
					return (PARSER_INCORRECT_NUMBER_OF_ARGS);
				}
					
				break;	
			}
			default:
			{
				return (PARSER_INCORRECT_NUMBER_OF_ARGS);
				break;
			}
		}
		
		if(k >= kmax)
		{
			return (PARSER_INCORRECT_NUMBER_OF_ARGS);
		}
	}
	return (PARSER_INCORRECT_NUMBER_OF_ARGS);
}

void setVCOM(void){ 
uint8_t res = getArgsPARSER(PARSER_PARSING_TYPE_DECIMAL,1);
	if(res == (PARSER_CORRECT_NUMBER_OF_ARGS))
	{
		if(chkArgRangePARSER() == (PARSER_ARG_IN_RANGE))
		{
				selectedVcom=&vcoms[g_PARSER_INFO.arg_int[0]];
				printUsb("\nCurrently selected VCOM%d\n",VCOM0,selectedVcom->id);
				}
		else
		{	
				printInvalidRangePARSER();
		}
	}
	else if(res == (PARSER_INCORRECT_NUMBER_OF_ARGS))
	{
		printInvalidRangePARSER();
	}
}


void setUSART(void){

uint8_t res = getArgsPARSER(PARSER_PARSING_TYPE_DECIMAL,1);
	if(res == (PARSER_CORRECT_NUMBER_OF_ARGS))
	{
		if(chkArgRangePARSER() == (PARSER_ARG_IN_RANGE))
		{
				// selectedVcom=&vcoms[g_PARSER_INFO.arg_int[0]];
				// printUsb("\nCurrently selected vcom[%d]",VCOM0,selectedVcom->id);
				selectedUart=&uarts[g_PARSER_INFO.arg_int[0]-1];
				printUsb("\nCurrently selected USART%d\n",VCOM0,selectedUart->id);


		}
		else
		{	
				printInvalidRangePARSER();
		}
	}
	else if(res == (PARSER_INCORRECT_NUMBER_OF_ARGS))
	{
		printInvalidRangePARSER();
	} 
}

void setSPI(void){

uint8_t res = getArgsPARSER(PARSER_PARSING_TYPE_DECIMAL,1);
	if(res == (PARSER_CORRECT_NUMBER_OF_ARGS))
	{
		if(chkArgRangePARSER() == (PARSER_ARG_IN_RANGE))
		{
				// selectedVcom=&vcoms[g_PARSER_INFO.arg_int[0]];
				// printUsb("\nCurrently selected vcom[%d]",VCOM0,selectedVcom->id);
				selectedSpi=&spis[g_PARSER_INFO.arg_int[0]-1];
				printUsb("\nCurrently selected SPI%d\n",VCOM0,selectedSpi->id);


		}
		else
		{	
				printInvalidRangePARSER();
		}
	}
	else if(res == (PARSER_INCORRECT_NUMBER_OF_ARGS))
	{
		printInvalidRangePARSER();
	} 
} 
void setBaudrate(void){

uint8_t res = getArgsPARSER(PARSER_PARSING_TYPE_DECIMAL,1);
	if(res == (PARSER_CORRECT_NUMBER_OF_ARGS))
	{
		if(chkArgRangePARSER() == (PARSER_ARG_IN_RANGE))
		{
				if(selectedUart==NULL){
					printUsb("\nUSART not selected!\n",VCOM0);
					return;
				} 
				uint32_t baudrate= g_PARSER_INFO.arg_int[0];
				if(baudrate!=115200 && baudrate!=921600 && baudrate!=9600 && baudrate!=19200 && baudrate!=57600){ 
					printUsb("\nPlease enter correct baudrate values ( 9600, 19200, 57600, 115200, 921600 )\n",VCOM0);
					return;
				}
				uint32_t realBaud=getRealBaud(selectedUart->id,baudrate);
				USART_TypeDef* usart=getUARTAdr(selectedUart->id);
				usart->BRR=realBaud;
				selectedUart->baudrate=baudrate;
				printUsb("\nBaudrate of USART%d changed to %d\n",VCOM0,selectedUart->id,baudrate);

		}
		else
		{	
				printInvalidRangePARSER();
		}
	}
	else if(res == (PARSER_INCORRECT_NUMBER_OF_ARGS))
	{
		printInvalidRangePARSER();
	} 
} 

void setPrescaler(void){
uint8_t res = getArgsPARSER(PARSER_PARSING_TYPE_DECIMAL,1);
	if(res == (PARSER_CORRECT_NUMBER_OF_ARGS))
	{
		if(chkArgRangePARSER() == (PARSER_ARG_IN_RANGE))
		{
				if(selectedSpi==NULL){
					printUsb("\nSPI not selected!\n",VCOM0);
					return;
				} 
				uint16_t prescaler= g_PARSER_INFO.arg_int[0];
				if(prescaler!=2 && prescaler!=4 && prescaler!=8 && prescaler!=16 && prescaler!=32 && prescaler!=64 && prescaler!=128 && prescaler!=256){ 
					printUsb("\nPlease enter correct prescaler values ( 2,4,8,16,32,64,128,256 )\n",VCOM0);
					return;
				}
				uint16_t realScale=getRealPrescaler(selectedSpi->id,prescaler);
				SPI_TypeDef* spi=getSPIAdr(selectedSpi->id);
				printUART("realScale = %d\n",realScale);
				if(spi==SPI1){
					printUART("SPI1 selected\n");
				}

				spi->CR1|=realScale;
				selectedSpi->prescaler=prescaler;
				printUsb("\nPrescaler of SPI%d changed to %d\n",VCOM0,selectedSpi->id,prescaler);

		}
		else
		{	
				printInvalidRangePARSER();
		}
	}
	else if(res == (PARSER_INCORRECT_NUMBER_OF_ARGS))
	{
		printInvalidRangePARSER();
	} 



}
void setCpol(void){
uint8_t res = getArgsPARSER(PARSER_PARSING_TYPE_DECIMAL,1);
	if(res == (PARSER_CORRECT_NUMBER_OF_ARGS))
	{
		if(chkArgRangePARSER() == (PARSER_ARG_IN_RANGE))
		{
				if(selectedSpi==NULL){
					printUsb("\nSPI not selected!\n",VCOM0);
					return;
				} 
				uint8_t cpol= g_PARSER_INFO.arg_int[0];
				SPI_TypeDef* spi=getSPIAdr(selectedSpi->id);
				if(cpol){
					spi->CR1|=(SPI_CR1_CPOL); 
				}else{
					spi->CR1&=~(SPI_CR1_CPOL); 
				}
				selectedSpi->cpol=cpol;
				printUsb("\nCPOL of SPI%d changed to %d\n",VCOM0,selectedSpi->id,cpol);

		}
		else
		{	
				printInvalidRangePARSER();
		}
	}
	else if(res == (PARSER_INCORRECT_NUMBER_OF_ARGS))
	{
		printInvalidRangePARSER();
	} 

}
void setCpha(void){
uint8_t res = getArgsPARSER(PARSER_PARSING_TYPE_DECIMAL,1);
	if(res == (PARSER_CORRECT_NUMBER_OF_ARGS))
	{
		if(chkArgRangePARSER() == (PARSER_ARG_IN_RANGE))
		{
				if(selectedSpi==NULL){
					printUsb("\nSPI not selected!\n",VCOM0);
					return;
				} 
				uint8_t cpha= g_PARSER_INFO.arg_int[0];
				SPI_TypeDef* spi=getSPIAdr(selectedSpi->id);
				if(cpha){
					spi->CR1|=(SPI_CR1_CPHA); 
				}else{
					spi->CR1&=~(SPI_CR1_CPHA); 
				}
				selectedSpi->cpha=cpha;
				printUsb("\nCPHA of SPI%d changed to %d\n",VCOM0,selectedSpi->id,cpha);

		}
		else
		{	
				printInvalidRangePARSER();
		}
	}
	else if(res == (PARSER_INCORRECT_NUMBER_OF_ARGS))
	{
		printInvalidRangePARSER();
	} 


} 
void setTimer(void){

uint8_t res = getArgsPARSER(PARSER_PARSING_TYPE_DECIMAL,1);
	if(res == (PARSER_CORRECT_NUMBER_OF_ARGS))
	{
		if(chkArgRangePARSER() == (PARSER_ARG_IN_RANGE))
		{
				selectedTimer=&timers[g_PARSER_INFO.arg_int[0]-1];
				printUsb("\nCurrently selected TIMER%d\n",VCOM0,selectedTimer->id); 
		}
		else
		{	
				printInvalidRangePARSER();
		}
	}
	else if(res == (PARSER_INCORRECT_NUMBER_OF_ARGS))
	{
		printInvalidRangePARSER();
	} 
} 
void setCCR1(void){

uint8_t res = getArgsPARSER(PARSER_PARSING_TYPE_DECIMAL,1);
	if(res == (PARSER_CORRECT_NUMBER_OF_ARGS))
	{
		if(chkArgRangePARSER() == (PARSER_ARG_IN_RANGE))
		{
				if(selectedTimer==NULL){
					printUsb("\nTimer not selected!\n",VCOM0);
					return;
				} 
				uint32_t value=g_PARSER_INFO.arg_int[0];
				selectedTimer->ccr1=value;
				selectedTimer->adr->CCR1=value;
				printUsb("\nCCR1 of TIMER%d changed to %d\n",VCOM0,selectedTimer->id,value);
		}
		else
		{	
				printInvalidRangePARSER();
		}
	}
	else if(res == (PARSER_INCORRECT_NUMBER_OF_ARGS))
	{
		printInvalidRangePARSER();
	} 
} 
void setCCR2(void){
uint8_t res = getArgsPARSER(PARSER_PARSING_TYPE_DECIMAL,1);
	if(res == (PARSER_CORRECT_NUMBER_OF_ARGS))
	{
		if(chkArgRangePARSER() == (PARSER_ARG_IN_RANGE))
		{
				if(selectedTimer==NULL){
					printUsb("\nTimer not selected!\n",VCOM0);
					return;
				} 
				uint32_t value=g_PARSER_INFO.arg_int[0];
				selectedTimer->ccr2=value;
				selectedTimer->adr->CCR2=value;
				printUsb("\nCCR2 of TIMER%d changed to %d\n",VCOM0,selectedTimer->id,value);
		}
		else
		{	
				printInvalidRangePARSER();
		}
	}
	else if(res == (PARSER_INCORRECT_NUMBER_OF_ARGS))
	{
		printInvalidRangePARSER();
	} 

};
void setCCR3(void){
uint8_t res = getArgsPARSER(PARSER_PARSING_TYPE_DECIMAL,1);
	if(res == (PARSER_CORRECT_NUMBER_OF_ARGS))
	{
		if(chkArgRangePARSER() == (PARSER_ARG_IN_RANGE))
		{
				if(selectedTimer==NULL){
					printUsb("\nTimer not selected!\n",VCOM0);
					return;
				} 
				uint32_t value=g_PARSER_INFO.arg_int[0];
				selectedTimer->ccr3=value;
				selectedTimer->adr->CCR3=value;
				printUsb("\nCCR3 of TIMER%d changed to %d\n",VCOM0,selectedTimer->id,value);
		}
		else
		{	
				printInvalidRangePARSER();
		}
	}
	else if(res == (PARSER_INCORRECT_NUMBER_OF_ARGS))
	{
		printInvalidRangePARSER();
	} 

};
void setCCR4(void){
uint8_t res = getArgsPARSER(PARSER_PARSING_TYPE_DECIMAL,1);
	if(res == (PARSER_CORRECT_NUMBER_OF_ARGS))
	{
		if(chkArgRangePARSER() == (PARSER_ARG_IN_RANGE))
		{
				if(selectedTimer==NULL){
					printUsb("\nTimer not selected!\n",VCOM0);
					return;
				} 
				uint32_t value=g_PARSER_INFO.arg_int[0];
				selectedTimer->ccr4=value;
				selectedTimer->adr->CCR4=value;
				printUsb("\nCCR4 of TIMER%d changed to %d\n",VCOM0,selectedTimer->id,value);
		}
		else
		{	
				printInvalidRangePARSER();
		}
	}
	else if(res == (PARSER_INCORRECT_NUMBER_OF_ARGS))
	{
		printInvalidRangePARSER();
	} 

};
void setPeriod(void){
uint8_t res = getArgsPARSER(PARSER_PARSING_TYPE_DECIMAL,1);
	if(res == (PARSER_CORRECT_NUMBER_OF_ARGS))
	{
		if(chkArgRangePARSER() == (PARSER_ARG_IN_RANGE))
		{
				if(selectedTimer==NULL){
					printUsb("\nTimer not selected!\n",VCOM0);
					return;
				} 
				uint32_t value=g_PARSER_INFO.arg_int[0];
				selectedTimer->period=value;
				selectedTimer->adr->ARR=value*10;
				printUsb("\nPeriod of TIMER%d changed to %dms\n",VCOM0,selectedTimer->id,value);
		}
		else
		{	
				printInvalidRangePARSER();
		}
	}
	else if(res == (PARSER_INCORRECT_NUMBER_OF_ARGS))
	{
		printInvalidRangePARSER();
	} 

};


void getVCOM(void){
uint8_t res = getArgsPARSER(PARSER_PARSING_TYPE_DECIMAL,1);
	if(res == (PARSER_CORRECT_NUMBER_OF_ARGS))
	{
		if(chkArgRangePARSER() == (PARSER_ARG_IN_RANGE))
		{
				uint8_t vcomId= g_PARSER_INFO.arg_int[0];
				printUsb("\nVCOM%d\n",VCOM0,vcomId);
				printUsb(" -TYPE =",VCOM0);
				switch(vcoms[vcomId].type){
					case NOTCONF_VCOM:
						printUsb(" NOT CONFIGURED\n",VCOM0);
						break;
					case USART_VCOM:
						printUsb(" USART\n",VCOM0);
						break;
					case SPI_VCOM:
						printUsb(" SPI\n",VCOM0);
						break; 
				}
				printUsb(" -PERIPHERAL NUMBER = %d\n",VCOM0,vcoms[vcomId].peripheralNum);
		}
		else
		{	
				printInvalidRangePARSER();
		}
	}
	else if(res == (PARSER_INCORRECT_NUMBER_OF_ARGS))
	{
		printInvalidRangePARSER();
	} 

}

void getSPI(void){
uint8_t res = getArgsPARSER(PARSER_PARSING_TYPE_DECIMAL,1);
	if(res == (PARSER_CORRECT_NUMBER_OF_ARGS))
	{
		if(chkArgRangePARSER() == (PARSER_ARG_IN_RANGE))
		{
				uint8_t spiId= g_PARSER_INFO.arg_int[0];
				printUsb("\nSPI%d\n",VCOM0,spiId);
				printUsb(" -PRESCALER = %d\n",VCOM0,spis[spiId-1].prescaler);
				printUsb(" -CPHA = %d\n",VCOM0,spis[spiId-1].cpha);
				printUsb(" -CPOL = %d\n",VCOM0,spis[spiId-1].cpol);
				printUsb(" -BOUND TO =",VCOM0);
				if(spis[spiId].boundVcom){
					printUsb(" VCOM%d\n",VCOM0,spis[spiId-1].boundVcom);
				}else{
					printUsb(" NONE\n",VCOM0);
				}
		}
		else
		{	
				printInvalidRangePARSER();
		}
	}
	else if(res == (PARSER_INCORRECT_NUMBER_OF_ARGS))
	{
		printInvalidRangePARSER();
	} 
}

void getUSART(void){
uint8_t res = getArgsPARSER(PARSER_PARSING_TYPE_DECIMAL,1);
	if(res == (PARSER_CORRECT_NUMBER_OF_ARGS))
	{
		if(chkArgRangePARSER() == (PARSER_ARG_IN_RANGE))
		{
				uint8_t usartId= g_PARSER_INFO.arg_int[0];
				printUsb("\nUSART%d\n",VCOM0,usartId);
				printUsb(" -BAUDRATE = %d\n",VCOM0,uarts[usartId-1].baudrate);
				printUsb(" -BOUND TO =",VCOM0);
				if(uarts[usartId-1].boundVcom){
					printUsb(" VCOM%d\n",VCOM0,uarts[usartId-1].boundVcom);
				}else{
					printUsb(" NONE\n",VCOM0);
				}
		}
		else
		{	
				printInvalidRangePARSER();
		}
	}
	else if(res == (PARSER_INCORRECT_NUMBER_OF_ARGS))
	{
		printInvalidRangePARSER();
	} 


}

void getStatus(void){

	printUsb("\n\033[31;1mSelected VCOM =",VCOM0);
	if(selectedVcom!=NULL){
		printUsb(" VCOM%d\033[0m\n",VCOM0,selectedVcom->id);
				printUsb(" VCOM%d\n",VCOM0,selectedVcom->id);
				printUsb("  -TYPE =",VCOM0);
				switch(selectedVcom->type){
					case NOTCONF_VCOM:
						printUsb(" NOT CONFIGURED\n",VCOM0);
						break;
					case USART_VCOM:
						printUsb(" USART\n",VCOM0);
						break;
					case SPI_VCOM:
						printUsb(" SPI\n",VCOM0);
						break; 
				}
				printUsb("  -PERIPHERAL NUMBER = %d\n",VCOM0,selectedVcom->peripheralNum);
	}else{
		printUsb(" NONE\033[0m\n",VCOM0); 
	}
	printUsb("\033[31;1mSelected USART =",VCOM0);
	if(selectedUart!=NULL){
				printUsb(" USART%d\033[0m\n",VCOM0,selectedUart->id);
				printUsb(" USART%d\n",VCOM0,selectedUart->id);
				printUsb("  -PRESCALER = %d\n",VCOM0,selectedUart->baudrate);
				printUsb("  -BOUND TO =",VCOM0);
				if(selectedUart->boundVcom){
					printUsb(" VCOM%d\n",VCOM0,selectedUart->boundVcom);
				}else{
					printUsb(" NONE\n",VCOM0);
				}
	}else{
		printUsb(" NONE\033[0m\n",VCOM0); 
	}
	printUsb("\033[31;1mSelected SPI =",VCOM0);
	if(selectedSpi!=NULL){
				printUsb(" SPI%d\033[0m\n",VCOM0,selectedSpi->id);
				printUsb(" SPI%d\n",VCOM0,selectedSpi->id);
				printUsb("  -PRESCALER = %d\n",VCOM0,selectedSpi->prescaler);
				printUsb("  -CPHA = %d\n",VCOM0,selectedSpi->cpha);
				printUsb("  -CPOL = %d\n",VCOM0,selectedSpi->cpol);
				printUsb("  -BOUND TO =",VCOM0);
				if(selectedSpi->boundVcom){
					printUsb(" VCOM%d\n",VCOM0,selectedSpi->boundVcom);
				}else{
					printUsb(" NONE\n",VCOM0);
				}
	}else{
		printUsb(" NONE\033[0m\n",VCOM0); 
	}
	printUsb("\033[31;1mSelected TIMER =",VCOM0);
	if(selectedTimer!=NULL){
				printUsb(" TIMER%d\033[0m\n",VCOM0,selectedTimer->id);
				printUsb(" TIMER%d\n",VCOM0,selectedTimer->id);
				printUsb("  -CCR1 = %d\n",VCOM0,selectedTimer->ccr1);
				printUsb("  -CCR2 = %d\n",VCOM0,selectedTimer->ccr2);
				printUsb("  -CCR3 = %d\n",VCOM0,selectedTimer->ccr3);
				printUsb("  -CCR4 = %d\n",VCOM0,selectedTimer->ccr4);
				printUsb("  -PERIOD = %dms\n",VCOM0,selectedTimer->period);
	}else{
		printUsb(" NONE\033[0m\n",VCOM0); 
	}

} 


void bindUSART(void){

uint8_t res = getArgsPARSER(PARSER_PARSING_TYPE_DECIMAL,1);
	if(res == (PARSER_CORRECT_NUMBER_OF_ARGS))
	{
		if(chkArgRangePARSER() == (PARSER_ARG_IN_RANGE))
		{
				if(selectedVcom==NULL){
					printUsb("\nVCOM not selected!\n",VCOM0);
					return;
				}
				if(selectedVcom->peripheralNum!=NONE_PER){
					printUsb("\nVCOM%d already bound\n",VCOM0,selectedVcom->id);
					return;
				}
				uint8_t usartId=g_PARSER_INFO.arg_int[0];
				if(uarts[usartId-1].boundVcom!=0){
					printUsb("\nUSART%d is already bound to VCOM%d\n",VCOM0,usartId,uarts[usartId-1].boundVcom);
				}
				else{

				selectedVcom->type=USART_VCOM;
				selectedVcom->peripheralNum=usartId;
				uarts[usartId-1].boundVcom=selectedVcom->id;
				printUsb("\nSelected USART%d -baudrate %d is bound to VCOM%d\n",VCOM0,usartId,uarts[usartId-1].baudrate,selectedVcom->id);
				if(selectedVcom->state!=NOTCON_STATE){ 
					printUsb("\033[34;1m------------------------\033[0m\n",selectedVcom->id);
					printUsb("VCOM%d - USART%d %d \n",selectedVcom->id,selectedVcom->id,usartId,uarts[usartId-1].baudrate);
					printUsb("\033[34;1m------------------------\033[0m\n",selectedVcom->id);
				switch(usartId){
					case 1: 
						enIrqUSART1(selectedVcom);
					break; 
					case 2: 
						enIrqUSART2(selectedVcom);
					break; 
				}
				}
				}

		}
		else
		{	
				printInvalidRangePARSER();
		}
	}
	else if(res == (PARSER_INCORRECT_NUMBER_OF_ARGS))
	{
		printInvalidRangePARSER();
	} 
}

void bindSPI(void){ 
	uint8_t res = getArgsPARSER(PARSER_PARSING_TYPE_DECIMAL,1);
		if(res == (PARSER_CORRECT_NUMBER_OF_ARGS))
		{
			if(chkArgRangePARSER() == (PARSER_ARG_IN_RANGE))
			{
					if(selectedVcom==NULL){
						printUsb("\nVcom not selected!\n",VCOM0);
						return;
					}
					if(selectedVcom->peripheralNum!=NONE_PER){
						printUsb("\nVCOM%d already bound!\n",VCOM0,selectedVcom->id);
						return;
					}
					uint8_t spiId=g_PARSER_INFO.arg_int[0];
					if(spis[spiId-1].boundVcom!=0){
						printUsb("\nSPI%d is already bound to VCOM%d\n",VCOM0,spiId-1,spis[spiId-1].boundVcom);
					}
					else{
						selectedVcom->type=SPI_VCOM;
						selectedVcom->peripheralNum=spiId;
						spis[spiId-1].boundVcom=selectedVcom->id; 
						printUsb("\nSelected SPI%d PRESCALER-%d CPOL-%d CPHA-%d is bound to VCOM%d\n",VCOM0,spiId,spis[spiId-1].prescaler,spis[spiId-1].cpol,spis[spiId-1].cpha,selectedVcom->id);
						if(selectedVcom->state!=NOTCON_STATE){ 
							printUsb("\033[32;1m--------------------------------------\033[0m\n",selectedVcom->id);
							printUsb("VCOM%d - SPI%d PRESCALER-%d CPOL-%d CPHA-%d\n",selectedVcom->id,selectedVcom->id,selectedVcom->peripheralNum,spis[selectedVcom->peripheralNum-1].prescaler,spis[selectedVcom->peripheralNum-1].cpol,spis[selectedVcom->peripheralNum-1].cpha);
							printUsb("\033[32;1m--------------------------------------\033[0m\n",selectedVcom->id);
						}


					}

			}
			else
			{	
					printInvalidRangePARSER();
			}
		}
		else if(res == (PARSER_INCORRECT_NUMBER_OF_ARGS))
		{
			printInvalidRangePARSER();
		} 
}


void unbindUSART(void){

uint8_t res = getArgsPARSER(PARSER_PARSING_TYPE_DECIMAL,1);
	if(res == (PARSER_CORRECT_NUMBER_OF_ARGS))
	{
		if(chkArgRangePARSER() == (PARSER_ARG_IN_RANGE))
		{
				uint8_t usartId=g_PARSER_INFO.arg_int[0];
				if(!(uarts[usartId-1].boundVcom)){
					printUsb("\nUSART/UART%d not bound to any VCOM!\n",VCOM0,usartId);
				}
				else{
				printUsb("\nSelected USART%d -baudrate %d is no longer bound to VCOM%d\n",VCOM0,usartId,uarts[usartId-1].baudrate,uarts[usartId-1].boundVcom);
				vcoms[uarts[usartId-1].boundVcom].peripheralNum=NONE_PER;
				vcoms[uarts[usartId-1].boundVcom].type=NOTCONF_VCOM; 
				if(vcoms[uarts[usartId-1].boundVcom].state!=NOTCON_STATE){
					notConfMsg(&vcoms[uarts[usartId-1].boundVcom]);
				}
				uarts[usartId-1].boundVcom=0;
				switch(usartId){
					case 1:
						disableIrqUSART1();
					case 2: 
						disableIrqUSART2();
					break; 
				}
				}

		}
		else
		{	
				printInvalidRangePARSER();
		}
	}
	else if(res == (PARSER_INCORRECT_NUMBER_OF_ARGS))
	{
		printInvalidRangePARSER();
	} 
}




void unbindSPI(){
uint8_t res = getArgsPARSER(PARSER_PARSING_TYPE_DECIMAL,1);
	if(res == (PARSER_CORRECT_NUMBER_OF_ARGS))
	{
		if(chkArgRangePARSER() == (PARSER_ARG_IN_RANGE))
		{
				uint8_t spiId=g_PARSER_INFO.arg_int[0];
				if(!(spis[spiId-1].boundVcom)){
					printUsb("\nSPI%d not bound to any VCOM!\n",VCOM0,spiId);
				}
				else{
				printUsb("\nSelected SPI%d is no longer bound to VCOM%d\n",VCOM0,spiId,spis[spiId-1].boundVcom);
				vcoms[spis[spiId-1].boundVcom].peripheralNum=NONE_PER;
				vcoms[spis[spiId-1].boundVcom].type=NOTCONF_VCOM; 
				if(vcoms[spis[spiId-1].boundVcom].state!=NOTCON_STATE){
					notConfMsg(&vcoms[spis[spiId-1].boundVcom]);
				}
				spis[spiId-1].boundVcom=0;
				}

		}
		else
		{	
				printInvalidRangePARSER();
		}
	}
	else if(res == (PARSER_INCORRECT_NUMBER_OF_ARGS))
	{
		printInvalidRangePARSER();
	} 

}
// void setVCOM2USART(void){ 
// 	if(usart2Flag){ 
// 		while(CDC_Transmit_FS("\r\nUSART2 already bound to another VCOM!\r\n",39,0)!=USBD_OK){};
// 	}
// 	else{
// 		while(CDC_Transmit_FS("\r\nVCOM2 as USART2 115200 configured!\r\n",39,0)!=USBD_OK){};
// 		vcoms[2].type=USART_VCOM;
// 		strcpy(vcoms[2].msg,"VCOM2 - USART2 115200");
// 		vcoms[2].msg_len=22;
// 		vcoms[2].peripheral=USART2_PER;
// 		usart2Flag=1;
// 		enIrqUSART2(&vcoms[2]);
// 	}

// }

// void resetUSART2(){
// 	if(usart2Flag){
// 		disableIrqUSART2();
// 		uint8_t i=0;
// 		for(i;i<MAX_VCOMS;i++){
// 			if(vcoms[i].peripheralNum==2){
// 				break;
// 			}
// 		} 
// 		vcoms[i].peripheralNum=NONE_PER;
//         // strcpy(vcoms[i].msg,"VCOM not configured!");
//         // vcoms[i].msg_len=39;
// 		vcoms[i].type=NOTCONF_VCOM;
// 		usart2Flag=0;
// 		while(CDC_Transmit_FS("\r\nUSART2 no longer bound to a VCOM\r\n",37,0)!=USBD_OK){};
// 	}else{ 
// 		while(CDC_Transmit_FS("\r\nUSART2 is not bound to a VCOM\r\n",34,0)!=USBD_OK){};

// 	}

// }

uint8_t chkArgRangePARSER(void)
{// input value is the argument index
	
	if((g_PARSER_INFO.arg_int[0] >= c_PARSER_CMDS[g_PARSER_INFO.cmd_idx].range_min)&&(g_PARSER_INFO.arg_int[0] <= c_PARSER_CMDS[g_PARSER_INFO.cmd_idx].range_max))
	{
		return (PARSER_ARG_IN_RANGE);
	}
	
	return (PARSER_ARG_NOT_IN_RANGE);
}	



uint8_t getNum4PARSER(void)
{
	uint32_t factors[10] = {1,10,100,1000,10000,100000,1000000,10000000,100000000,1000000000};
	uint32_t base;
	uint16_t len;
	uint8_t k;
	
	
	//printUART(" I[%s].F[%s]\n",g_PARSER_INFO.str_before_dot,g_PARSER_INFO.str_after_dot);
	len = lenStrMISC((uint8_t *)g_PARSER_INFO.str_before_dot);
	
	if(len == 0)
	{
		return (PARSER_VALUE_OUT_OF_RANGE);
	}
	
	if(len <= 10)
	{
		base = factors[len-1];
		for(k=0;k<len;k++)
		{
			g_PARSER_INFO.arg_int[0] += base*((uint32_t)(g_PARSER_INFO.str_before_dot[k] - 0x30));
			base = base/10;
		}
	}
	else
	{
		return (PARSER_VALUE_OUT_OF_RANGE);
	}
	
	g_PARSER_INFO.str_after_dot[3] = 0;
	len = lenStrMISC((uint8_t *)g_PARSER_INFO.str_after_dot);
	if(len > 0)
	{
		base = factors[len-1];
		for(k=0;k<len;k++)
		{
			g_PARSER_INFO.arg_frac[0] += base*((uint32_t)(g_PARSER_INFO.str_after_dot[k] - 0x30));
			base = base/10;
		}
	}
		
	return (PARSER_CORRECT_NUMBER_OF_ARGS);
}
