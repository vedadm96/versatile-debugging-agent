#include "usart.h"
#include "terminal.h"

volatile uint8_t irqFlag;
vcom* usart2VCOM;
vcom* usart1VCOM;
extern vcom vcoms[MAX_VCOMS];



void putcharUSARTx(USART_TypeDef* usart,uint8_t data){
	while(!(usart->SR & USART_SR_TC));									
	usart->DR = data;												    
}








void initUSART1(uint32_t baudrate){
	// USART1: PB6 -> TX	PB7 -> RX
	RCC->AHB1ENR |= RCC_AHB1ENR_GPIOBEN;
	RCC->APB2ENR |= RCC_APB2ENR_USART1EN;
	GPIOB->MODER &= (GPIO_MODER_MODER6);
	GPIOB->MODER |=(GPIO_MODER_MODER6_1)|(GPIO_MODER_MODER7_1);
	GPIOB->AFR[0] &= ~0xFF000000; 
	GPIOB->AFR[0] |=  0x77000000;

	GPIOB->OSPEEDR |= (GPIO_OSPEEDER_OSPEEDR6_1);
	GPIOB->OSPEEDR |= (GPIO_OSPEEDER_OSPEEDR7_1);

	USART1->BRR = baudrate;
	USART1->CR1=(USART_CR1_UE)|(USART_CR1_TE);

}
void enIrqUSART1(vcom* currentVCOM)
{
	usart1VCOM=currentVCOM;
	USART1->CR1 &= ~(USART_CR1_UE);
	
	NVIC_EnableIRQ(USART1_IRQn);
	USART1->CR1 |= (USART_CR1_UE)|(USART_CR1_RE)|(USART_CR1_RXNEIE);
}
void disableIrqUSART1(){
	NVIC_DisableIRQ(USART1_IRQn);
}
void USART1_IRQHandler(void)
{
	if(usart1VCOM->state!=INIT_STATE && usart1VCOM->state!=NOTCON_STATE){
	if(USART1->SR&(USART_SR_RXNE))
	{
		USART1->SR &= ~(USART_SR_RXNE);
		usart1VCOM->recBuffer[usart1VCOM->recBufferSize++]=USART1->DR;
		usart1VCOM->state=REC_STATE;
	}
	}
	// putcharUSART2(data);

}

void initUSART2(uint32_t baudrate){
	// USART2: PA2 -> TX	PA3 -> RX
	RCC->AHB1ENR |= RCC_AHB1ENR_GPIOAEN;
	RCC->APB1ENR |= RCC_APB1ENR_USART2EN;
	GPIOA->MODER &= (GPIO_MODER_MODER2);
	GPIOA->MODER |=(GPIO_MODER_MODER2_1)|(GPIO_MODER_MODER3_1);
	GPIOA->AFR[0] &= ~0x0000FF00; 
	GPIOA->AFR[0] |=  0x00007700;

	GPIOA->OSPEEDR |= (GPIO_OSPEEDER_OSPEEDR2_1);
	GPIOA->OSPEEDR |= (GPIO_OSPEEDER_OSPEEDR3_1);

	USART2->BRR = baudrate;
	USART2->CR1=(USART_CR1_UE)|(USART_CR1_TE);

}
void enIrqUSART2(vcom* currentVCOM)
{
	usart2VCOM=currentVCOM;
	USART2->CR1 &= ~(USART_CR1_UE);
	
	NVIC_EnableIRQ(USART2_IRQn);
	USART2->CR1 |= (USART_CR1_UE)|(USART_CR1_RE)|(USART_CR1_RXNEIE);
}
void disableIrqUSART2(){
	NVIC_DisableIRQ(USART2_IRQn);
}
void USART2_IRQHandler(void)
{
	if(usart2VCOM->state!=INIT_STATE && usart2VCOM->state!=NOTCON_STATE){
	if(USART2->SR&(USART_SR_RXNE))
	{
		USART2->SR &= ~(USART_SR_RXNE);
		usart2VCOM->recBuffer[usart2VCOM->recBufferSize++]=USART2->DR;
		usart2VCOM->state=REC_STATE;
	}
	}
	// putcharUSART2(data);

}




void initUSART3(uint32_t baudrate)
{
	//wwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwww
	// USART3: PC10 -> TX       PC11 -> RX
	//------------------------------------------------------------------ 
	RCC->AHB1ENR |= RCC_AHB1ENR_GPIOCEN; 									
	RCC->APB1ENR |= RCC_APB1ENR_USART3EN; 	
	GPIOC->MODER &= ~(GPIO_MODER_MODER10);								
	GPIOC->MODER |= (GPIO_MODER_MODER10_1)|(GPIO_MODER_MODER11_1); 	
	GPIOC->AFR[1] &= ~0x0000FF00;		
	GPIOC->AFR[1] |=  0x00007700;										
	
	GPIOC->OSPEEDR |= (GPIO_OSPEEDER_OSPEEDR10_1);	
	GPIOC->OSPEEDR |= (GPIO_OSPEEDER_OSPEEDR11_1);	
															
	USART3->BRR = baudrate;
	USART3->CR1 = (USART_CR1_UE)|(USART_CR1_TE);							
}
  uint8_t getcharUSART3() {
  while (!(USART3->SR & USART_SR_RXNE))
    ;
  return USART3->DR;
}
void enIrqUSART3(void)
{
	USART3->CR1 &= ~(USART_CR1_UE);
	
	NVIC_EnableIRQ(USART3_IRQn);
	USART3->CR1 |= (USART_CR1_UE)|(USART_CR1_RE)|(USART_CR1_RXNEIE);
}
void USART3_IRQHandler(void)
{
	uint8_t data;

	if(USART3->SR&(USART_SR_RXNE))
	{
		// USART3->DR = USART3->DR;
		USART3->SR &= ~(USART_SR_RXNE);
		USART3->DR = data;
	}
	putcharUSART3(data);
	irqFlag=0x1;
}
void putcharUSART2(uint8_t data)
{/// print one character to USART2
	uint32_t k;
	
	for(k=0;k<(USART_WAIT4TC_COUNT);k++)
	{
		if(USART2->SR & USART_SR_TC)
		{	
			USART2->DR = data;	
			break;	
		}
	}												    
}
void putcharUSART3(uint8_t data)
{/// print one character to USART3
	uint32_t k;
	
	for(k=0;k<(USART_WAIT4TC_COUNT);k++)
	{
		if(USART3->SR & USART_SR_TC)
		{	
			USART3->DR = data;	
			break;	
		}
	}												    
}

void printUSART3(char *str, ... )
{ /// print text and up to 10 arguments!
    uint8_t rstr[40];													// 33 max -> 32 ASCII for 32 bits and NULL 
    uint16_t k = 0;
	uint16_t arg_type;
	uint32_t utmp32;
	uint32_t * p_uint32; 
	char * p_char;
	va_list vl;
	
	//va_start(vl, 10);													// always pass the last named parameter to va_start, for compatibility with older compilers
	va_start(vl, str);													// always pass the last named parameter to va_start, for compatibility with older compilers
	while(str[k] != 0x00)
	{
		if(str[k] == '%')
		{
			if(str[k+1] != 0x00)
			{
				switch(str[k+1])
				{
					case('b'):
					{// binary
						if(str[k+2] == 'b')
						{// byte
							utmp32 = va_arg(vl, int);
							arg_type = (PRINT_ARG_TYPE_BINARY_BYTE);
						}
						else if(str[k+2] == 'h')
						{// half word
							utmp32 = va_arg(vl, int);
							arg_type = (PRINT_ARG_TYPE_BINARY_HALFWORD);
						}
						else if(str[k+2] == 'w')
						{// word	
							utmp32 = va_arg(vl, uint32_t);
							arg_type = (PRINT_ARG_TYPE_BINARY_WORD);
						}
						else
						{// default word
							utmp32 = va_arg(vl, uint32_t);
							arg_type = (PRINT_ARG_TYPE_BINARY_WORD);
							k--;
						}
						
						k++;	
						p_uint32 = &utmp32;
						break;
					}
					case('d'):
					{// decimal
						if(str[k+2] == 'b')
						{// byte
							utmp32 = va_arg(vl, int);
							arg_type = (PRINT_ARG_TYPE_DECIMAL_BYTE);
						}
						else if(str[k+2] == 'h')
						{// half word
							utmp32 = va_arg(vl, int);
							arg_type = (PRINT_ARG_TYPE_DECIMAL_HALFWORD);
						}
						else if(str[k+2] == 'w')
						{// word	
							utmp32 = va_arg(vl, uint32_t);
							arg_type = (PRINT_ARG_TYPE_DECIMAL_WORD);
						}
						else
						{// default word
							utmp32 = va_arg(vl, uint32_t);
							arg_type = (PRINT_ARG_TYPE_DECIMAL_WORD);
							k--;
						}
						
						k++;	
						p_uint32 = &utmp32;
						break;
					}
					case('c'):
					{// character
						char tchar = va_arg(vl, int);	
						putcharUSART3(tchar);
						arg_type = (PRINT_ARG_TYPE_CHARACTER);
						break;
					}
					case('s'):
					{// string 
						p_char = va_arg(vl, char *);	
						sprintUSART3((uint8_t *)p_char);
						arg_type = (PRINT_ARG_TYPE_STRING);
						break;
					}
					case('f'):
					{// float
						uint64_t utmp64 = va_arg(vl, uint64_t);			// convert double to float representation IEEE 754
						uint32_t tmp1 = utmp64&0x00000000FFFFFFFF;
						tmp1 = tmp1>>29;
						utmp32 = utmp64>>32;
						utmp32 &= 0x07FFFFFF;
						utmp32 = utmp32<<3;
						utmp32 |= tmp1;
						if(utmp64 & 0x8000000000000000)
							utmp32 |= 0x80000000;
							
						if(utmp64 & 0x4000000000000000)
							utmp32 |= 0x40000000;
							
						p_uint32 = &utmp32;
						
						arg_type = (PRINT_ARG_TYPE_FLOAT);
						//arg_type = (PRINT_ARG_TYPE_HEXADECIMAL_WORD);
						//arg_type = (PRINT_ARG_TYPE_BINARY_WORD);
						break;
					}
					case('x'):
					{// hexadecimal 
						if(str[k+2] == 'b')
						{// byte
							utmp32 = (uint32_t)va_arg(vl, int);
							arg_type = (PRINT_ARG_TYPE_HEXADECIMAL_BYTE);
						}
						else if(str[k+2] == 'h')
						{// half word
							utmp32 = (uint32_t)va_arg(vl, int);
							arg_type = (PRINT_ARG_TYPE_HEXADECIMAL_HALFWORD);
						}
						else if(str[k+2] == 'w')
						{// word	
							utmp32 = va_arg(vl, uint32_t);
							arg_type = (PRINT_ARG_TYPE_HEXADECIMAL_WORD);
						}
						else
						{
							utmp32 = va_arg(vl, uint32_t);
							arg_type = (PRINT_ARG_TYPE_HEXADECIMAL_WORD);
							k--;
						}
						
						k++;
						p_uint32 = &utmp32;
						break;
					}
					default:
					{
						utmp32 = 0;
						p_uint32 = &utmp32;
						arg_type = (PRINT_ARG_TYPE_UNKNOWN);
						break;
					}
				}
					
				if(arg_type&(PRINT_ARG_TYPE_MASK_CHAR_STRING))	
				{
					getStr4NumMISC(arg_type, p_uint32, rstr);
					sprintUSART3(rstr);	
				}
				k++;
			}
		}
		else
		{// not a '%' char -> print the char
			putcharUSART3(str[k]);
			if (str[k] == '\n')
				putcharUSART3('\r');
		}
		k++;
	}
	
	va_end(vl);
	return;
}

void sprintUSART3(uint8_t * str)
{
	uint16_t k = 0;
	
	while (str[k] != '\0')
    {
		//if(k<2)
		//{
			//putcharUSART3('\\');
			//putcharUSART3('e');
			//putcharUSART3('[');
			//putcharUSART3('3');
			//putcharUSART3('4');
			//putcharUSART3('m');
			
			//putcharUSART3(str[k]);
			
			//putcharUSART3('\\');
			//putcharUSART3('e');
			//putcharUSART3('[');
			//putcharUSART3('3');
			//putcharUSART3('9');
			//putcharUSART3('m');
			
		//}
		//else
		//{
			putcharUSART3(str[k]);
		//}
		
        //putcharUSART3(str[k]);
        if (str[k] == '\n')
            putcharUSART3('\r');
        k++;

        if (k == MAX_PRINT_STRING_SIZE)
            break;
    }
}

uint32_t getRealBaud(uint8_t uartNum,uint32_t baudrate){
	uint32_t realBaud=0;
	switch(baudrate){
		case 115200 :
			if(uartNum==1){
				realBaud=USART1_BAUDRATE_115200;
			}else{
				realBaud=USART2_BAUDRATE_115200; 
			}
		break;
		case 921600 :
			if(uartNum==1){
				realBaud=USART1_BAUDRATE_921600;
			}else{
				realBaud=USART2_BAUDRATE_921600; 
			}
		break;
		case 9600 :
			if(uartNum==1){
				realBaud=USART1_BAUDRATE_9600;
			}else{
				realBaud=USART2_BAUDRATE_9600; 
			}
		break;
		case 19200 :
			if(uartNum==1){
				realBaud=USART1_BAUDRATE_19200;
			}else{
				realBaud=USART2_BAUDRATE_19200; 
			}
		break;
		case 57600 :
			if(uartNum==1){
				realBaud=USART1_BAUDRATE_57600;
			}else{
				realBaud=USART2_BAUDRATE_57600; 
			}
		break;


	} 

	return realBaud; 
}