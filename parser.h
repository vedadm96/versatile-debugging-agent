#pragma once
#include "config.h"
#include "usart.h"
#include "delay.h"




#define PARSER_BUFFER_SIZE 2048
#define PARSER_BUFFER_MASK ((PARSER_BUFFER_SIZE-1))
#define UART_PARSER_BUFFER_MASK ((UART_BUFFER_SIZE)-1)


#define PARSER_CMD_COUNT					23
#define PARSER_CMD_SIZE						128

#define PARSER_IDLE							0x00
#define PARSER_BUFFER_OVERFLOW				0x01
#define PARSER_CMD_CAPTURED					0x02

#define PARSER_CMD_HELP						0x00 
#define PARSER_CMD_BIND_SPI					0x01
#define PARSER_CMD_BIND_USART				0x02
#define PARSER_CMD_GET_SPI					0x03
#define PARSER_CMD_GET_STATUS				0x04
#define PARSER_CMD_GET_USART				0x05
#define PARSER_CMD_GET_VCOM					0x06
#define PARSER_CMD_SET_BAUDRATE				0x07
#define PARSER_CMD_SET_CCR1					0x08
#define PARSER_CMD_SET_CCR2					0x09
#define PARSER_CMD_SET_CCR3					0x0A
#define PARSER_CMD_SET_CCR4					0x0B
#define PARSER_CMD_SET_CPHA					0x0C
#define PARSER_CMD_SET_CPOL					0x0D
#define PARSER_CMD_SET_PERIOD				0x0E
#define PARSER_CMD_SET_PRESCALER			0x0F
#define PARSER_CMD_SET_SPI					0x10
#define PARSER_CMD_SET_TIMER				0x11
#define PARSER_CMD_SET_USART				0x12
#define PARSER_CMD_SET_VCOM					0x13
#define PARSER_CMD_UNBIND_SPI				0x14
#define PARSER_CMD_UNBIND_USART				0x15
#define PARSER_CMD_GET_ECOMP				0x0A
#define PARSER_CMD_GET_ENOISE				0x0B
#define PARSER_CMD_GET_FREQ					0x0C
#define PARSER_CMD_GET_FS					0x0D
#define PARSER_CMD_GET_LIST					0x0E
#define PARSER_CMD_GET_PH					0x0F
#define PARSER_CMD_GET_RMSAHEADRX			0x10
#define PARSER_CMD_GET_RMSBACKRX			0x11
#define PARSER_CMD_GET_RMSDELAYRX			0x12
#define PARSER_CMD_GET_RMSAHEADTX			0x13
#define PARSER_CMD_GET_RMSBACKTX			0x14
#define PARSER_CMD_GET_RMSDELAYTX			0x15
#define PARSER_CMD_REBOOT					0x16
#define PARSER_CMD_SET_HELP					0x17
#define PARSER_CMD_SET_AMP					0x18
#define PARSER_CMD_SET_AGCATTACK			0x19
#define PARSER_CMD_SET_AGCMAX				0x1A
#define PARSER_CMD_SET_AGCRELEASE			0x1B
#define PARSER_CMD_SET_ATTEN				0x1C
#define PARSER_CMD_SET_BUFLENRX				0x1D
#define PARSER_CMD_SET_BUFLENTX				0x1E
#define PARSER_CMD_SET_ECLIP				0x1F
#define PARSER_CMD_SET_ECOMP				0x20
#define PARSER_CMD_SET_ENOISE				0x21
#define PARSER_CMD_SET_FREQ					0x22
#define PARSER_CMD_SET_FS					0x23
#define PARSER_CMD_SET_PH					0x24
#define PARSER_CMD_SET_RMSAHEADRX			0x25
#define PARSER_CMD_SET_RMSBACKRX			0x26
#define PARSER_CMD_SET_RMSDELAYRX			0x27
#define PARSER_CMD_SET_RMSAHEADTX			0x28
#define PARSER_CMD_SET_RMSBACKTX			0x29
#define PARSER_CMD_SET_RMSDELAYTX			0x2A
#define PARSER_CMD_TRIGEDIV					0x2B
#define PARSER_CMD_TRIGENABLE				0x2C
#define PARSER_CMD_SET_MODE					0x2D
#define PARSER_CMD_SET_COMP					0x2E
#define PARSER_CMD_GET_COMP					0x2F
#define PARSER_CMD_GET_TRIGDIV				0x30
#define PARSER_CMD_GET_TRIGEN				0x31
#define PARSER_CMD_GET_ALGORXPROCTIME		0x32
#define PARSER_CMD_GET_ALGOTXPROCTIME		0x33
#define PARSER_CMD_SET_ALGORXDAC1SRC		0x34
#define PARSER_CMD_SET_ALGORXDAC2SRC		0x35
#define PARSER_CMD_SET_ALGOTXDAC1SRC		0x36
#define PARSER_CMD_SET_ALGOTXDAC2SRC		0x37
#define PARSER_CMD_GET_ALGORXDAC1SRC		0x38
#define PARSER_CMD_GET_ALGORXDAC2SRC		0x39
#define PARSER_CMD_GET_ALGOTXDAC1SRC		0x3A
#define PARSER_CMD_GET_ALGOTXDAC2SRC		0x3B
#define PARSER_CMD_GET_MODE					0x3C

#define PARSER_CMD_GET_AT1RX				0x3D
#define PARSER_CMD_GET_AT2RX				0x3E
#define PARSER_CMD_GET_AT1TX				0x3F
#define PARSER_CMD_GET_AT2TX				0x40
#define PARSER_CMD_GET_AT3TX				0x41
#define PARSER_CMD_GET_AT1BL				0x42
#define PARSER_CMD_GET_AT2BL				0x43
#define PARSER_CMD_GET_AT3BL				0x44
#define PARSER_CMD_GET_AT4BL				0x45
#define PARSER_CMD_SET_AT1RX				0x46
#define PARSER_CMD_SET_AT2RX				0x47
#define PARSER_CMD_SET_AT1TX				0x48
#define PARSER_CMD_SET_AT2TX				0x49
#define PARSER_CMD_SET_AT3TX				0x4A
#define PARSER_CMD_SET_AT1BL				0x4B
#define PARSER_CMD_SET_AT2BL				0x4C
#define PARSER_CMD_SET_AT3BL				0x4D
#define PARSER_CMD_SET_AT4BL				0x4E
#define PARSER_CMD_LOAD_SDCARD				0x4F
#define PARSER_CMD_SAVE_SDCARD				0x50




#define PARSER_CORRECT_NUMBER_OF_ARGS		0x00
#define PARSER_INCORRECT_NUMBER_OF_ARGS		0x01
#define PARSER_VALUE_OUT_OF_RANGE			0x02
#define PARSER_RANGE_HELP					0x03

#define PARSER_FIND_FIRST_SPACE				0x00
#define PARSER_FIND_FIRST_NONSPACE			0x01
#define PARSER_FIND_SECOND_SPACE			0x02
#define PARSER_SEARCH4DATA					0x03
#define PARSER_CAPTURE_DATA					0x04
#define PARSER_GENERATE_NUMBERS				0x05

#define PARSER_FIND_SECOND_SPACE			0x02
#define PARSER_NOT_DIGIT					0x00
#define PARSER_IS_DIGIT						0x01
#define PARSER_DIGIT_STRING_LEN				10

#define PARSER_MSG_MISSING_ARG				0x00
#define PARSER_MSG_WRONG_ARG				0x01


#define PARSER_PARSING_TYPE_DECIMAL			0x01
#define PARSER_PARSING_TYPE_HEX				0x02
#define PARSER_PARSING_TYPE_STRING			0x04
#define PARSER_PARSING_TYPE_FLOAT			0x08

#define PARSER_TYPE_INT						0
#define PARSER_TYPE_FLOAT1K					1
#define PARSER_TYPE_FLOAT1					2

#define PARSER_ARG_NOT_IN_RANGE				0
#define PARSER_ARG_IN_RANGE					1

#define PRASER_LIST_ITEM_LENGTH				18
#define PARSER_TABED_CMD_LEN				22
	

#define PARSER_EXE_CMD_LIST_SIZE 23
#define PARSER_BUFFER_SIZE 2048
#define PARSER_TBUFFER_SIZE 2048
struct parser
{
	uint16_t get_cmd_bidx;
	uint16_t get_cmd_eidx;
	uint16_t set_cmd_bidx;
	uint16_t set_cmd_eidx;
	uint8_t update_flag;
	uint16_t skip_str_parsing; 
	uint16_t uart_widx; 
	uint16_t uart_ridx;
	uint16_t widx; 
	uint16_t ridx;
	uint16_t tab_cnt;
	uint16_t exe_cmd_ridx;
	uint16_t exe_cmd_widx;
	uint16_t exe_cmd_pidx;
	uint8_t exe_cmd_list[PARSER_EXE_CMD_LIST_SIZE][PARSER_BUFFER_SIZE];
	uint8_t spec_char;
	uint8_t exe_cmd_from_list;
	uint8_t* tbuff;
	uint8_t buff[PARSER_BUFFER_SIZE];
	uint16_t update_param_timer;
	uint16_t update_param_idx;
	uint8_t uart_buff[UART_BUFFER_SIZE];
	void (*update_param_fnc)(void);
	uint16_t cmd_len;
	uint16_t cmd_idx;
    uint8_t	str_before_dot[16]; 
	uint8_t	str_after_dot[16]; 
	uint32_t arg_int[16];
	uint8_t arg_frac[16];
};




typedef struct parser_cmds_t
{
	uint8_t cmd[PARSER_CMD_SIZE];
	uint8_t cmd_id;
	void (*fnc)(void);
	
	uint32_t range_min;
	uint32_t range_max;
	
	uint8_t data_type;
	 
} PARSER_CMDS;





void initPARSER(void);
void clrBuffPARSER(void);
void chkPARSER(void);

void printCmdRangePRASER(void);
void printTerminalPrefixPARSER(void);
void printCmdNotFoundPARSER(void);
void printInvalidRangePARSER(void);
void printCmdRangePARSER(void);


void helpCmdPARSER(void);
void getHelpCmdPARSER(void);
void getAmpCmdPARSER(void);
void getAgcAttackCmdPARSER(void);
void getAttenCmdPARSER(void);
void getAt1BlCmdPARSER(void);
void getAt2BlCmdPARSER(void);
void getAt3BlCmdPARSER(void);
void getAt4BlCmdPARSER(void);
void getAt1RxCmdPARSER(void);
void getAt2RxCmdPARSER(void);
void getAt1TxCmdPARSER(void);
void getAt2TxCmdPARSER(void);
void getAt3TxCmdPARSER(void);
void getAgcMaxCmdPARSER(void);
void getAgcReleaseCmdPARSER(void);
void getAlgoRxDac1SrcCmdPARSER(void);
void getAlgoRxDac2SrcCmdPARSER(void);
void getAlgoRxProcTimeCmdPARSER(void);
void getAlgoTxDac1SrcCmdPARSER(void);
void getAlgoTxDac2SrcCmdPARSER(void);
void getAlgoTxProcTimeCmdPARSER(void);
void getBuffLenRxCmdPARSER(void);
void getBuffLenTxCmdPARSER(void);
void getCompCmdPARSER(void);
void getEclipCmdPARSER(void);
void getEcompCmdPARSER(void);
void getEnoiseCmdPARSER(void);
void getFreqCmdPARSER(void);
void getFsCmdPARSER(void);
void getListCmdPARSER(void);
void getModeCmdPARSER(void);
void getPhCmdPARSER(void);
void getRmsAheadRxCmdPARSER(void);
void getRmsBackRxCmdPARSER(void);
void getRmsDelayRxCmdPARSER(void);
void getRmsAheadTxCmdPARSER(void);
void getRmsBackTxCmdPARSER(void);
void getRmsDelayTxCmdPARSER(void);
void getTrigDivCmdPARSER(void);
void getTrigEnCmdPARSER(void);

void resetCmdPARSER(void);

void setHelpCmdPARSER(void);
void setAmpCmdPARSER(void);
void setAgcAttackCmdPARSER(void);
void setAlgoRxDac1SrcCmdPARSER(void);
void setAlgoRxDac2SrcCmdPARSER(void);
void setAlgoTxDac1SrcCmdPARSER(void);
void setAlgoTxDac2SrcCmdPARSER(void);
void setAttenCmdPARSER(void);

void setAt1BlCmdPARSER(void);
void setAt2BlCmdPARSER(void);
void setAt3BlCmdPARSER(void);
void setAt4BlCmdPARSER(void);
void setAt1RxCmdPARSER(void);
void setAt2RxCmdPARSER(void);
void setAt1TxCmdPARSER(void);
void setAt2TxCmdPARSER(void);
void setAt3TxCmdPARSER(void);
uint8_t getSpacePARSER(uint8_t lvl);

void setAgcMaxCmdPARSER(void);
void setAgcReleaseCmdPARSER(void);
void setBuffLenRxCmdPARSER(void);
void setBuffLenTxCmdPARSER(void);
void setCompCmdPARSER(void);
void setEclipCmdPARSER(void);
void setEcompCmdPARSER(void);
void setEnoiseCmdPARSER(void);
void setFreqCmdPARSER(void);
void setFsCmdPARSER(void);
void setModePARSER(void);
void setListCmdPARSER(void);
void setPhCmdPARSER(void);
void setRmsAheadRxCmdPARSER(void);
void setRmsBackRxCmdPARSER(void);
void setRmsDelayRxCmdPARSER(void);
void setRmsAheadTxCmdPARSER(void);
void setRmsBackTxCmdPARSER(void);
void setRmsDelayTxCmdPARSER(void);


void setVCOM(void);
void setUSART(void);
void setSPI(void);
void bindUSART(void);
void bindSPI(void);
void unbindUSART(void);
void unbindSPI(void);
void getVCOM(void);
void getUSART(void);
void getSPI(void);
void getStatus(void);


void setBaudrate(void);
void setPrescaler(void);
void setCpol(void);
void setCpha(void);
void setTimer(void);
void setCCR1(void);
void setCCR2(void);
void setCCR3(void);
void setCCR4(void);
void setPeriod(void);

void setTrigDivCmdPARSER(void);
void setTrigEnCmdPARSER(void);

void loadCmdPARSER(void);
void saveCmdPARSER(void);

uint8_t chkArgRangePARSER(void);

uint8_t getArgsPARSER(uint8_t pars_type, uint8_t num_args);
uint8_t getNum4PARSER(void);
uint8_t getCmdIdxPARSER(uint8_t idx);
void printFloatPARSER(uint8_t * str, uint32_t res_sharing_idx, uint32_t scale_factor);
void printListItemFixedPosPRASER(uint8_t * str, uint32_t val, uint8_t type, uint32_t scale);

void printMsgFromIndPARSER(uint8_t idx);

void printIndMsgPARSER(uint8_t * data, uint8_t type);


void addParserUARTBuffer(char* buff,uint16_t len);