#pragma once
#include "stm32f4xx.h"
#include "usbd_cdc_if.h"

#define VCOM_INPUT_BUFFER_SIZE 512
#define VCOM_OUTPUT_BUFFER_SIZE 2048
#define VCOM_REC_BUFFER_SIZE 512
#define MSG_SIZE 50
#define MAX_VCOMS 3
#define MAX_UARTS 2
#define MAX_SPIS 2
#define MAX_TIMERS 3


#define VCOM0 0
#define VCOM1 1
#define VCOM2 2
//vcom states
#define NOTCON_STATE 0
#define REC_STATE 4
#define INPUT_STATE 3
#define INIT_STATE 1
#define IDLE_STATE 2
//vcom types
#define CONTROL_VCOM 0
#define USART_VCOM 1
#define SPI_VCOM 2
#define NOTCONF_VCOM 3
//vcom peripherals

#define NONE_PER 0


typedef struct vcom vcom;

struct vcom{
    uint8_t id;
    uint8_t state;
    uint8_t type;
    uint8_t inputBuffer[VCOM_INPUT_BUFFER_SIZE];
    uint8_t outputBuffer[VCOM_OUTPUT_BUFFER_SIZE];
    uint8_t recBuffer[VCOM_REC_BUFFER_SIZE];
    uint16_t outputBufferSize;
    uint16_t inputBufferSize;
    uint16_t recBufferSize;
    uint8_t peripheralNum;
    uint8_t hexFlag;
};

typedef struct uart{
	uint8_t id;
	uint32_t baudrate;
	uint8_t boundVcom; 
} uart;

typedef struct spi{
	uint8_t id;
	uint32_t prescaler;
	uint8_t boundVcom;
    uint8_t cpol;
    uint8_t cpha;
} spi;

typedef struct timerU{
    uint8_t id;
    uint16_t period;
    uint16_t ccr1;
    uint16_t ccr2;
    uint16_t ccr3;
    uint16_t ccr4;
    TIM_TypeDef* adr;    

}timerU;
void initVCOMS();
void initUARTS();
void initSPIS();
void initTIMERS();

void checkUsbOutput();
void copyToOutputBuffer(vcom* currentVCOM,uint8_t* buf,uint16_t len);
void initControlVCOM(vcom* confVCOM);
void initUsartVCOM(vcom* currentVCOM);
void VCOMRecieveFS(vcom* currentVCOM,uint8_t* Buf,uint32_t* Len);
void handleVCOMType(vcom* currentVCOM);
static void handleStateControl(vcom* currentVCOM);
static void handleStateUsart(vcom* currentVCOM);
static void handleStateSpi(vcom* currentVCOM);

//UART
void addToInputBuffer(vcom* VCOM,uint8_t* buff,uint32_t len);
void processRecBuffer(vcom* VCOM);
void processUartInputBuffer(vcom* currentVCOM); 
USART_TypeDef* getUARTAdr(uint8_t id);
SPI_TypeDef* getSPIAdr(uint8_t id);


//SPI
void initSpiVCOM(vcom* currentVCOM);
void processSpiInputBuffer(vcom* currentVCOM);
void printSpiInputBuffer(vcom* currentVCOM); 
void sendSpiData(vcom* currentVCOM);
void sendReadSpiData(vcom* currentVCOM);

//usb printf
void printUsb(char* str,uint8_t epnum,...);
void sprintVcom(uint8_t* str, uint8_t epnum);
void putcharVcom(char chr,uint8_t epnum);
void notConfMsg(vcom * currentVCOM);