#include "terminal.h"
#include "usart.h"
#include "parser.h"
#include "spi.h"
extern uart uarts[MAX_UARTS];
extern vcom vcoms[MAX_VCOMS];
extern spi spis[MAX_SPIS];
extern timerU timers[MAX_TIMERS];
extern struct parser g_PARSER_INFO;


void initVCOMS(){
    for(uint8_t i=1; i<MAX_VCOMS;i++){
        vcoms[i].state=NOTCON_STATE;
        vcoms[i].id=i;
        vcoms[i].type=NOTCONF_VCOM;
        vcoms[i].peripheralNum=NONE_PER;
        vcoms[i].outputBufferSize=0;
        vcoms[i].inputBufferSize=0;
        vcoms[i].recBufferSize=0;
        vcoms[i].hexFlag=0;
    }
    vcoms[0].id=0;
    vcoms[0].type=CONTROL_VCOM;
    vcoms[0].state=NOTCON_STATE;
    vcoms[0].outputBufferSize=0;
	vcoms[0].inputBufferSize=0;
	vcoms[0].recBufferSize=0;


}

void initUARTS(){
	for(uint8_t i=0;i<MAX_UARTS;i++){
		uarts[i].id=i+1;
		uarts[i].baudrate=921600;
		uarts[i].boundVcom=0; 
	} 
}
void initSPIS(){
	for(uint8_t i=0;i<MAX_SPIS;i++){
		spis[i].id=i+1;
		spis[i].prescaler=256;
		spis[i].boundVcom=0; 
		spis[i].cpha=0;
		spis[i].cpol=0;
	} 
}
void initTIMERS(){
	for(uint8_t i=0;i<MAX_TIMERS;i++){
		timers[i].id=i+1;
		timers[i].ccr1=0;
		timers[i].ccr2=0;
		timers[i].ccr3=0;
		timers[i].ccr4=0;
		timers[i].period=1;
	} 
	timers[0].adr=TIM1;
	timers[1].adr=TIM2;
	timers[2].adr=TIM3;

}



void checkUsbOutput(){
    for(uint8_t i=0;i<MAX_VCOMS;i++){
        if(vcoms[i].outputBufferSize){
            printUART("vcom[%d] outputBuffersize=%d\n",i,vcoms[i].outputBufferSize);
            if(CDC_Transmit_FS(vcoms[i].outputBuffer,vcoms[i].outputBufferSize,i)==USBD_OK){
                vcoms[i].outputBufferSize=0;
            }
        }
    } 
}

void copyToOutputBuffer(vcom* currentVCOM,uint8_t* buf,uint16_t len){
    for(uint8_t i=0;i<len;i++){
        currentVCOM->outputBuffer[currentVCOM->outputBufferSize++]=buf[i];
    } 
}



void initControlVCOM(vcom* confVCOM){
     confVCOM->state=IDLE_STATE;
     delay_ms(100);
     initPARSER();
}
void initUsartVCOM(vcom* currentVCOM){
    printUART("initUsartVCOM id=%d\n",currentVCOM->id);
    currentVCOM->state=IDLE_STATE;
    currentVCOM->inputBufferSize=0; 
    currentVCOM->recBufferSize=0; 
	currentVCOM->outputBufferSize=0;
	printUsb("\033[34;1m------------------------\033[0m\n",currentVCOM->id);
	printUsb("VCOM%d - USART%d %d \n",currentVCOM->id,currentVCOM->id,currentVCOM->peripheralNum,uarts[currentVCOM->peripheralNum-1].baudrate);
	printUsb("\033[34;1m------------------------\033[0m\n",currentVCOM->id);
	switch(currentVCOM->peripheralNum){
		case 1:
			enIrqUSART1(currentVCOM);
			break;
		case 2:
			enIrqUSART2(currentVCOM);
			break;
		default:
		break;
	}
}

void initSpiVCOM(vcom* currentVCOM){
	printUART("initSpiVCOM id=%d\n",currentVCOM->id);
	currentVCOM->state=IDLE_STATE;
	currentVCOM->inputBufferSize=0;
	currentVCOM->recBufferSize=0; 
	printUsb("\033[32;1m---------------------------------\033[0m\n",currentVCOM->id);
	printUsb("VCOM%d - SPI%d PRESCALER-%d CPOL-%d CPHA-%d\n",currentVCOM->id,currentVCOM->id,currentVCOM->peripheralNum,spis[currentVCOM->peripheralNum-1].prescaler,spis[currentVCOM->peripheralNum-1].cpol,spis[currentVCOM->peripheralNum-1].cpha);
	printUsb("\033[32;1m---------------------------------\033[0m\n",currentVCOM->id);


}
void VCOMRecieveFS(vcom* currentVCOM,uint8_t* Buf,uint32_t* Len){
	printUART("VCOMRecieveFS\n");
    switch(currentVCOM->type){ 
        case CONTROL_VCOM:
        addParserUARTBuffer(Buf,*Len);
        currentVCOM->state=INPUT_STATE;
            break;
        case USART_VCOM:
			if(Buf[0]==27){
				currentVCOM->hexFlag^=1;
			}else{
				// printUART("\n");
				addToInputBuffer(currentVCOM,Buf,*Len);
				currentVCOM->state=INPUT_STATE;
			}
            break;
        case SPI_VCOM:
			if((Buf[0]>='0' && Buf[0]<='9') || (Buf[0]>='a' && Buf[0]<='f') || (Buf[0]>='A' && Buf[0]<='F')){
				if(Buf[0]>='a' && Buf[0]<='f')
					Buf[0]-=0x20;
				addToInputBuffer(currentVCOM,Buf,*Len);
				putcharVcom(Buf[0],currentVCOM->id);
			}else{
				switch(Buf[0]){
					case '\r':
						if(currentVCOM->inputBufferSize && (currentVCOM->inputBufferSize)%2)
							currentVCOM->inputBufferSize--; 
						printSpiInputBuffer(currentVCOM);
						for(uint16_t i=0;i<currentVCOM->inputBufferSize;i++){
							putcharVcom(currentVCOM->inputBuffer[i],currentVCOM->id);
						}
					break;
					case 0x7f:
						if(currentVCOM->inputBufferSize){
								currentVCOM->inputBufferSize--; 
						printUsb("\b \b",currentVCOM->id);
						}
					break;
					case 't':
					case 'T':
						if(currentVCOM->inputBufferSize && (currentVCOM->inputBufferSize)%2)
							currentVCOM->inputBufferSize--; 
						printSpiInputBuffer(currentVCOM);
						sendSpiData(currentVCOM);
						for(uint16_t i=0;i<currentVCOM->inputBufferSize;i++){
							putcharVcom(currentVCOM->inputBuffer[i],currentVCOM->id);
						}
					break;
					case 'r':
					case 'R':
						sendReadSpiData(currentVCOM);
						printUsb("\n",currentVCOM->id);
						currentVCOM->inputBufferSize=0;
					break;
					case 0x1B:
						printUsb("\n",currentVCOM->id);
						currentVCOM->inputBufferSize=0;
					break;
			}
			}
            break;
	}
}


void notConfMsg(vcom* currentVCOM){
// delay_ms(100);
printUART("notConfMsg state=%d\n",currentVCOM->state);
printUsb("\n\033[31;1m------------------------\033[0m\n",currentVCOM->id);	
printUsb("VCOM%d not configured!",currentVCOM->id,currentVCOM->id);
printUsb("\n\033[31;1m------------------------\033[0m\n",currentVCOM->id);	

currentVCOM->state=IDLE_STATE;

}


void handleVCOMType(vcom* currentVCOM){
    // printUART("handleVCOMType type=%d\n",currentVCOM->type);
    switch(currentVCOM->type){
        case CONTROL_VCOM:
            handleStateControl(currentVCOM);
            break;
        case USART_VCOM:
            handleStateUsart(currentVCOM);
            break;
        case SPI_VCOM:
            handleStateSpi(currentVCOM);
            break;
        case NOTCONF_VCOM:
            notConfMsg(currentVCOM);
            break;
        }

}
static void handleStateUsart(vcom* currentVCOM){
    // printUART("handleStateUsart state=%d\n",currentVCOM->state);
       switch(currentVCOM->state){
            case INIT_STATE:
                initUsartVCOM(currentVCOM);
            break;
            case INPUT_STATE: 
				processUartInputBuffer(currentVCOM);
				currentVCOM->state=IDLE_STATE;
            break;
            case REC_STATE:
                processRecBuffer(currentVCOM);
				currentVCOM->state=IDLE_STATE; 
            break;
            case IDLE_STATE:
            break;
			case NOTCON_STATE:
			break;


       } 
}


static void handleStateSpi(vcom* currentVCOM){ 
       switch(currentVCOM->state){
            case INIT_STATE:
				initSpiVCOM(currentVCOM);
            break;
            case INPUT_STATE: 
				// processSpiInputBuffer(currentVCOM);
				currentVCOM->state=IDLE_STATE;
            break;
            case REC_STATE:
            break;
            case IDLE_STATE:
            break;
			case NOTCON_STATE:
			break; 
       } 
}


static void handleStateControl(vcom* confVCOM){
    // printUART("handleStateControl\n"); 
    switch(confVCOM->state){
        case INIT_STATE:
            initControlVCOM(confVCOM); 
        break;
        case IDLE_STATE:
        break;
        case INPUT_STATE:
            chkPARSER();
            confVCOM->state=IDLE_STATE;
        break;

    } 

}


void addToInputBuffer(vcom* VCOM,uint8_t* buff,uint32_t len){
	// printUART("addToInputBuffer\n");
    for(uint16_t i=0;i<len;i++){
        VCOM->inputBuffer[VCOM->inputBufferSize++]=buff[i];
    } 
}
void processRecBuffer(vcom* VCOM){
    // printUART("recBufferSize=%d\n",VCOM->recBufferSize);
	if(VCOM->hexFlag){
     for(uint16_t i=0;i<VCOM->recBufferSize;i++){
		 	printUsb("<0x%xb>",VCOM->id,VCOM->recBuffer[i]);
	 }
	}
	else{
		copyToOutputBuffer(VCOM,VCOM->recBuffer,VCOM->recBufferSize);
	}
    VCOM->recBufferSize=0;
}
void processUartInputBuffer(vcom* currentVCOM){
	USART_TypeDef* resUSART=getUARTAdr(currentVCOM->peripheralNum);
	printUsb("\033[31;1m",currentVCOM->id);
	for(uint16_t i=0;i<currentVCOM->inputBufferSize;i++){
		putcharUSARTx(resUSART,currentVCOM->inputBuffer[i]);
		putcharVcom(currentVCOM->inputBuffer[i],currentVCOM->id);
		if(currentVCOM->inputBuffer[i]=='\r'){
			putcharVcom('\n',currentVCOM->id); 
		}
	}
	printUsb("\033[0m",currentVCOM->id);
	currentVCOM->inputBufferSize=0; 
}

//could replace with struct member in SPI/USART
USART_TypeDef* getUARTAdr(uint8_t num){
	switch(num){
		case 1:
			return USART1;
		case 2:
			return USART2;
	}
	return NULL; 
}
SPI_TypeDef* getSPIAdr(uint8_t id){
	switch(id){
		case 1:
			return SPI1;
		case 2:
			return SPI2;
	}
	return NULL; 
}


void processSpiInputBuffer(vcom* currentVCOM){


	for(uint16_t i=0;i<currentVCOM->inputBufferSize;i++){
		putcharVcom(currentVCOM->inputBuffer[i],currentVCOM->id);
	}
	currentVCOM->inputBufferSize=0; 
}

void printSpiInputBuffer(vcom* currentVCOM){
	uint8_t binaryOutput[VCOM_INPUT_BUFFER_SIZE];
	printUsb("\n[",currentVCOM->id); 
	for(uint16_t i=0;i<currentVCOM->inputBufferSize;i+=2){
		uint8_t sum=0;
		if(currentVCOM->inputBuffer[i]>='0' && currentVCOM->inputBuffer[i]<='9'){
			sum+=(currentVCOM->inputBuffer[i])-48; 
		}else{ 
			sum+=(currentVCOM->inputBuffer[i])-55; 
		}
		sum=sum<<4;
		if(currentVCOM->inputBuffer[i+1]>='0' && currentVCOM->inputBuffer[i+1]<='9'){
			sum+=(currentVCOM->inputBuffer[i+1])-48; 
		}else{ 
			sum+=(currentVCOM->inputBuffer[i+1])-55; 
		}
		printUsb(" %c%c ",currentVCOM->id,currentVCOM->inputBuffer[i],currentVCOM->inputBuffer[i+1]);
		binaryOutput[i/2]=sum;
	}
	printUsb("]\n",currentVCOM->id); 
	putcharVcom('[',currentVCOM->id);
	for(uint16_t i=0;i<currentVCOM->inputBufferSize/2;i++){
		printUsb(" %bb ",currentVCOM->id,binaryOutput[i]);

	}
	printUsb("]\n",currentVCOM->id); 

}
void sendSpiData(vcom* currentVCOM){
	uint8_t binaryOutput[VCOM_INPUT_BUFFER_SIZE];
	for(uint16_t i=0;i<currentVCOM->inputBufferSize;i+=2){
		uint8_t sum=0;
		if(currentVCOM->inputBuffer[i]>='0' && currentVCOM->inputBuffer[i]<='9'){
			sum+=(currentVCOM->inputBuffer[i])-48; 
		}else{ 
			sum+=(currentVCOM->inputBuffer[i])-55; 
		}
		sum=sum<<4;
		if(currentVCOM->inputBuffer[i+1]>='0' && currentVCOM->inputBuffer[i+1]<='9'){
			sum+=(currentVCOM->inputBuffer[i+1])-48; 
		}else{ 
			sum+=(currentVCOM->inputBuffer[i+1])-55; 
		}
		binaryOutput[i/2]=sum;
	}
	for(uint16_t i=0;i<currentVCOM->inputBufferSize/2;i++){
		printUART("%xb ",binaryOutput[i]);	
	}
	SPI_TypeDef* spi=getSPIAdr(currentVCOM->peripheralNum);
	if(currentVCOM->peripheralNum==1){
		SPI1_CS_LOW;
	}else{
		SPI2_CS_LOW; 
	}
	for(uint16_t i=0;i<currentVCOM->inputBufferSize/2;i++){
		txByteSPIx(spi,binaryOutput[i]);	
	}
	if(currentVCOM->peripheralNum==1){
		SPI1_CS_HIGH;
	}else{
		SPI2_CS_HIGH; 
	}

} 
void sendReadSpiData(vcom* currentVCOM){
	printUART("sendReadSpiData\n");
	uint8_t binaryOutput[VCOM_INPUT_BUFFER_SIZE];
	for(uint16_t i=0;i<currentVCOM->inputBufferSize;i+=2){
		uint8_t sum=0;
		if(currentVCOM->inputBuffer[i]>='0' && currentVCOM->inputBuffer[i]<='9'){
			sum+=(currentVCOM->inputBuffer[i])-48; 
		}else{ 
			sum+=(currentVCOM->inputBuffer[i])-55; 
		}
		sum=sum<<4;
		if(currentVCOM->inputBuffer[i+1]>='0' && currentVCOM->inputBuffer[i+1]<='9'){
			sum+=(currentVCOM->inputBuffer[i+1])-48; 
		}else{ 
			sum+=(currentVCOM->inputBuffer[i+1])-55; 
		}
		binaryOutput[i/2]=sum;
	}
	for(uint16_t i=0;i<currentVCOM->inputBufferSize/2;i++){
		printUART("%xb ",binaryOutput[i]);	
	}
	SPI_TypeDef* spi=getSPIAdr(currentVCOM->peripheralNum);
	if(currentVCOM->peripheralNum==1){
		SPI1_CS_LOW;
	}else{
		SPI2_CS_LOW; 
	}
	for(uint16_t i=0;i<currentVCOM->inputBufferSize/2;i++){
		txByteSPIx(spi,binaryOutput[i]);	
	}
	uint8_t data = rxByteSPIx(spi); 
	if(currentVCOM->peripheralNum==1){
		SPI1_CS_HIGH;
	}else{
		SPI2_CS_HIGH; 
	}
	printUsb("\n\033[31;1m MISO [ %xb ]\n[ %bb ]\033[0m",currentVCOM->id,data,data);


} 


void putcharVcom(char chr,uint8_t epnum){
    vcoms[epnum].outputBuffer[vcoms[epnum].outputBufferSize++]=chr;
} 

void printUsb(char * str,uint8_t epnum, ...)
{
    uint8_t rstr[40];													// 33 max -> 32 ASCII for 32 bits and NULL 
    uint16_t k = 0;
	uint16_t arg_type;
	uint32_t utmp32;
	uint32_t * p_uint32; 
	char * p_char;
	va_list vl;
	
	//va_start(vl, 10);													// always pass the last named parameter to va_start, for compatibility with older compilers
	va_start(vl, epnum);													// always pass the last named parameter to va_start, for compatibility with older compilers
	while(str[k] != 0x00)
	{
		if(str[k] == '%')
		{
			if(str[k+1] != 0x00)
			{
				switch(str[k+1])
				{
					case('b'):
					{// binary
						if(str[k+2] == 'b')
						{// byte
							utmp32 = va_arg(vl, int);
							arg_type = (PRINT_ARG_TYPE_BINARY_BYTE);
						}
						else if(str[k+2] == 'h')
						{// half word
							utmp32 = va_arg(vl, int);
							arg_type = (PRINT_ARG_TYPE_BINARY_HALFWORD);
						}
						else if(str[k+2] == 'w')
						{// word	
							utmp32 = va_arg(vl, uint32_t);
							arg_type = (PRINT_ARG_TYPE_BINARY_WORD);
						}
						else
						{// default word
							utmp32 = va_arg(vl, uint32_t);
							arg_type = (PRINT_ARG_TYPE_BINARY_WORD);
							k--;
						}
						
						k++;	
						p_uint32 = &utmp32;
						break;
					}
					case('d'):
					{// decimal
						if(str[k+2] == 'b')
						{// byte
							utmp32 = va_arg(vl, int);
							arg_type = (PRINT_ARG_TYPE_DECIMAL_BYTE);
						}
						else if(str[k+2] == 'h')
						{// half word
							utmp32 = va_arg(vl, int);
							arg_type = (PRINT_ARG_TYPE_DECIMAL_HALFWORD);
						}
						else if(str[k+2] == 'w')
						{// word	
							utmp32 = va_arg(vl, uint32_t);
							arg_type = (PRINT_ARG_TYPE_DECIMAL_WORD);
						}
						else
						{// default word
							utmp32 = va_arg(vl, uint32_t);
							arg_type = (PRINT_ARG_TYPE_DECIMAL_WORD);
							k--;
						}
						
						k++;	
						p_uint32 = &utmp32;
						break;
					}
					case('c'):
					{// character
						char tchar = va_arg(vl, int);	
						// putcharUSART3(tchar);
                        putcharVcom(tchar,epnum);
						arg_type = (PRINT_ARG_TYPE_CHARACTER);
						break;
					}
					case('s'):
					{// string 
						// p_char = va_arg(vl, char *);	
						// sprintUSART3((uint8_t *)p_char);
						// arg_type = (PRINT_ARG_TYPE_STRING);
						// break;
					}
					case('f'):
					{// float
						uint64_t utmp64 = va_arg(vl, uint64_t);			// convert double to float representation IEEE 754
						uint32_t tmp1 = utmp64&0x00000000FFFFFFFF;
						tmp1 = tmp1>>29;
						utmp32 = utmp64>>32;
						utmp32 &= 0x07FFFFFF;
						utmp32 = utmp32<<3;
						utmp32 |= tmp1;
						if(utmp64 & 0x8000000000000000)
							utmp32 |= 0x80000000;
							
						if(utmp64 & 0x4000000000000000)
							utmp32 |= 0x40000000;
							
						p_uint32 = &utmp32;
						
						arg_type = (PRINT_ARG_TYPE_FLOAT);
						//arg_type = (PRINT_ARG_TYPE_HEXADECIMAL_WORD);
						//arg_type = (PRINT_ARG_TYPE_BINARY_WORD);
						break;
					}
					case('x'):
					{// hexadecimal 
						if(str[k+2] == 'b')
						{// byte
							utmp32 = (uint32_t)va_arg(vl, int);
							arg_type = (PRINT_ARG_TYPE_HEXADECIMAL_BYTE);
						}
						else if(str[k+2] == 'h')
						{// half word
							utmp32 = (uint32_t)va_arg(vl, int);
							arg_type = (PRINT_ARG_TYPE_HEXADECIMAL_HALFWORD);
						}
						else if(str[k+2] == 'w')
						{// word	
							utmp32 = va_arg(vl, uint32_t);
							arg_type = (PRINT_ARG_TYPE_HEXADECIMAL_WORD);
						}
						else
						{
							utmp32 = va_arg(vl, uint32_t);
							arg_type = (PRINT_ARG_TYPE_HEXADECIMAL_WORD);
							k--;
						}
						
						k++;
						p_uint32 = &utmp32;
						break;
					}
					default:
					{
						utmp32 = 0;
						p_uint32 = &utmp32;
						arg_type = (PRINT_ARG_TYPE_UNKNOWN);
						break;
					}
				}
					
				if(arg_type&(PRINT_ARG_TYPE_MASK_CHAR_STRING))	
				{
					getStr4NumMISC(arg_type, p_uint32, rstr);
					sprintVcom(rstr,epnum);	
				}
				k++;
			}
		}
		else
		{// not a '%' char -> print the char
			// putcharUSART3(str[k]);
            putcharVcom(str[k],epnum);
			if (str[k] == '\n'){
				// putcharUSART3('\r'); 
                putcharVcom('\r',epnum);
            }
		}
		k++;
	}
	
	va_end(vl);
	return;





}

void sprintVcom(uint8_t * str,uint8_t epnum)
{
	uint16_t k = 0;
	
	while (str[k] != '\0')
    {
		//if(k<2)
		//{
			//putcharUSART3('\\');
			//putcharUSART3('e');
			//putcharUSART3('[');
			//putcharUSART3('3');
			//putcharUSART3('4');
			//putcharUSART3('m');
			
			//putcharUSART3(str[k]);
			
			//putcharUSART3('\\');
			//putcharUSART3('e');
			//putcharUSART3('[');
			//putcharUSART3('3');
			//putcharUSART3('9');
			//putcharUSART3('m');
			
		//}
		//else
		//{
			putcharVcom(str[k],epnum);
		//}
		
        //putcharUSART3(str[k]);
        if (str[k] == '\n')
            putcharVcom('\r',epnum);
        k++;

        if (k == MAX_PRINT_STRING_SIZE)
            break;
    }
}